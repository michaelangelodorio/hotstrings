
import Container from 'react-bootstrap/Container'
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { useState, useEffect } from 'react';
import AppNavbar from './components/global/AppNavbar';
import Home from './pages/Home'
import ProductView from './pages/ProductView';
import Carts from './pages/Carts';
import Orders from './pages/Orders';
import Register from './pages/Register'
import Login from './pages/Login';
import Logout from './pages/Logout';
import Profile from './pages/Profile';
import Error from './pages/Error';
import AppBar from './components/global/AppBar'
import Footer from './components/global/Footer'
import { UserProvider } from './UserContext';
// import { CartProvider } from './CartContext';
import './App.css';
import Users from './pages/Users';
import Products from './pages/Products';
import Contact from './pages/Contact';

function App() {
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState(null);

  const [user, setUser] = useState({
    //token: localStorage.getItem('token')
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    // Simulate AI processing
    setTimeout(() => {
      // Fetch data or perform AI-related tasks here
      fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
        headers: {
          Authorization: `Bearer ${ localStorage.getItem('token')}`
        }
      })
      .then(res => res.json())
      .then(data => {
        // Set the user state values with the user details upon successful login.
        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        }
        else{
          setUser({
            id: null,
            isAdmin: null
          })
        } 
      })

      setIsLoading(false);
    }, 600); // Simulate 2 seconds of processing time
  }, [user]);

  return (
    
    <div>
      {isLoading ? (
         <div className="bg-dark centered-container">
         <img width={100}
           src="https://usagif.com/wp-content/uploads/loading-12.gif"
           alt="Centered Image"
           className="centered-image"
         />
       </div>
      ) : (
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
          <Container fluid id="_body">
            <Routes>
              <Route path="/" element={<Home/>}/>
              <Route path="/products" element={<Products/>}/>
              <Route path="/products/:productId" exact="true" element={<ProductView/>}/>
              <Route path="/users" element={<Users/>}/>
              <Route path="/users/profile" element={<Profile/>}/>
              <Route path="/carts" element={<Carts />}/>
              <Route path="/orders" element={<Orders/>}/>
              <Route path="/contact" element={<Contact/>}/>
              <Route path="/users/register" element={<Register/>}/>
              <Route path="/users/login" element={<Login/>}/>
              <Route path="/logout" element={<Logout/>}/>
              <Route path="/*" element={<Error/>} />
            </Routes>
            <AppNavbar/>
            <AppBar/>
            <Footer />
          </Container>
        </Router> 
      </UserProvider>
      )}
    </div>
  );
}

export default App;
