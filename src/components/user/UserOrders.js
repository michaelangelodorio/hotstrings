import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import FormatDate from '../global/FormatDate';
import Accordion from 'react-bootstrap/Accordion';
import FormatCurrency from '../global/FormatCurrency';
import UserContext from '../../UserContext';
import EditUserOrder from './EditUserOrder';
import { Card, Container, Row, Col, Tab, Tabs, Button, Modal, Table} from 'react-bootstrap';
import Products from '../../pages/Products';

export default function UserOrders({ orderId, status, fetchOrderData, order }) {
    const {user, setUser} = useContext(UserContext);
    const [myOrders, setOrders] = useState([]);
    const [ordersCount, setOrdersCount] = useState(0);
    const [error, setError] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState(null);
    const [showEdit, setShowEdit] = useState(false);
    const [selectedSuborder, setSelectedSuborder] = useState(null);
    
    useEffect(() => {
        // Simulate AI processing
        setTimeout(() => {
          // Fetch data or perform AI-related tasks here
          setData(/* your fetched data or AI result */);
          setIsLoading(false);
        }, 2000); // Simulate 2 seconds of processing time
  
        if (showEdit) {
          fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`
            }
          })
            .then((res) => res.json())
            .then((data) => {
              fetchOrderData();
            });
        }
        if (order !== undefined) {
          setOrders(order);
          const activeItems = myOrders.filter(order => order.userId === user.id && order.isActive === true)
          const inactiveItems = myOrders.filter(order => order.userId === user.id && order.isActive === false ).length;
          setOrdersCount(activeItems.length)
        } else { 
          setOrders([]);
          setOrdersCount(0);
        }

    }, [fetchOrderData]);


// Function to open the modal for a specific order
const openEdit = (suborder) => {
  setShowEdit({ ...showEdit, [suborder._id]: true });
  fetchOrderData();
};

// Function to close the modal for a specific order
const closeEdit = () => {
  fetchOrderData();
  setShowEdit(false);
};

    const [sortByLatest, setSortByLatest] = useState(true);

    const sortedOrders = myOrders
      .slice() // Create a copy of myOrders to avoid mutating the original array
      .sort((a, b) => {
        const dateA = new Date(a.orderDate);
        const dateB = new Date(b.orderDate);
        return sortByLatest ? dateB - dateA : dateA - dateB;
      });

      const handleOpenOrder = async () => {
            closeEdit();
      };
    
    return (
      <div>
        {isLoading ? (
          <div className="bg-dark centered-container">
            <img
              width={100}
              src="https://usagif.com/wp-content/uploads/loading-12.gif"
              alt="Centered Image"
              className="centered-image"
            />
          </div>
        ) : (
          <Container fluid className="mt-3">
            <Row>
              <Col md={12}>
                <div className="page-header  text-center text-warning my-5">
                  <h1>
                    <span className="text-light">Order History</span>
                  </h1>
                  <h4>
                    Orders<span> ({ordersCount})</span>
                  </h4>
                  <Button onClick={() => setSortByLatest(!sortByLatest)}>
                  <i class="fa-solid fa-sort fa-fade"></i> {sortByLatest ? 'Sort Oldest to Latest' : 'Sort Latest to Oldest'}
                  </Button>
                  <div>
              
                  </div>
                </div>
                <div id="orders" className="d-flex flex-wrap justify-content-center">

                {sortedOrders.reduce((uniqueOrders, orders) => {
                            const existingOrder = uniqueOrders.find((p) => p._id === orders._id);

                            if (existingOrder) {
                              existingOrder.count += 1;
                            } else {
                              uniqueOrders.push({ ...orders, count: 1 });
                            }
                            return uniqueOrders;
                          }, [])
                          .filter((order) => order.count > 0 && order.userId === user.id)
                          .map((suborder) => (
                      <div key={suborder._id} className="mb-4">
                        <div className="d-flex border bg-dark m-2 p-2 text-white productHighlight">
                          <Container>
                            <Row>
                              <Col md={6} lg={6}>
                                <ul className="me-5">         
                                  
                                  <li className="pt-2"><strong>Name:</strong> {suborder.fullName}</li>
                                  <li className="pt-2"><strong>Mobile:</strong> {suborder.mobileNo}</li>
                                  <li className="pt-2"><strong>Address:</strong> {suborder.address}</li>
                                  <li className="pt-2"><strong>Total Amount:</strong> {FormatCurrency(suborder.totalAmount)}</li>
                                  <li className="pt-2"><strong>Total Items:</strong> {suborder.totalItems}</li>

                                  {suborder.userActions === "Cancelled" || suborder.userActions === "Completed" ? (
                                    <li className="pt-2 h5 hidden"><strong>Status:</strong> {suborder.orderStatus}</li>
                                  ) : null}
                                  
                                  <li className={`pt-2 h5 ${suborder.userActions === "Cancelled" ? 'text-red' : 'text-warning'}`}><strong>User Actions:</strong> {suborder.userActions}</li>
                           
                                </ul>
                                </Col>
                                <Col md={6}  lg={84}>
                                <ul>
                                  <li className="pt-2 h4">Orders: <Link Link title="Show Orders" className='m-1 px-3' variant="warning" size="sm" onClick={() => openEdit(suborder)}>
                                  <i class="fa-solid fa-basket-shopping fa-fade"></i>
                                  </Link></li>
                                  <li className="pt-2"><strong>Ref:</strong> {suborder.referenceId}</li>
                                  <li className="pt-2"><strong>Order Date:</strong></li>
                                  <li className="pt-2">{suborder.orderDate.toLocaleString()}</li>
                                  {suborder.userActions === "Completed" ? (
                                    <li className="pt-2 h5 text-info"><strong>Status:</strong> {suborder.userActions}</li>
                                  ) : null}

                              </ul>
                              </Col>
                              <Modal show={showEdit[suborder._id]} onHide={() => closeEdit(suborder)}>
                              <Modal.Header closeButton>
                              <Modal.Title>Order History</Modal.Title>
                              </Modal.Header>

                              <Modal.Body className='bg-dark'>
                              <ul>

                              {suborder.products.map((product) => (
                              <div key={product._id} eventKey={product._id} className="p-1 m-1 bg-dark text-light">
                              <Container fluid>
                                <Row>
                                  <Col xs={4}>
                                  
                                    <img className="img-fluid imgthumbnail-sm" src={product.productImg} alt={product.name} />
                                  </Col>
                                  <Col xs={8}>
                                    <li><span>{product.name}</span></li>
                                    <li><span>Quantity: {product.quantity}</span></li>
                                    <li><span>Subtotal: ${product.subtotal}</span></li>
                                  </Col>
                                </Row>
                              </Container>
                                </div>
                              ))}
                              {!(suborder.orderStatus === 'Cancelled' || suborder.orderStatus === 'Completed') ? (
                                <EditUserOrder
                                  order={suborder}  // Changed 'order' to 'suborder'
                                  orderId={suborder._id}
                                  fetchOrderData={fetchOrderData}
                                  isActive={suborder.isActive}
                                />   
                                ) : (
                                <div className="p-3 my-1">
                                  <h4>
                                    <span className={`px-3 bg-info bg-opacity-10 border border-info border-start-0 rounded text-${suborder.orderStatus === 'Cancelled' ? 'danger' : 'success'}`}>
                                      {suborder.orderStatus}
                                    </span>
                                  </h4>
                                  <p>For any inquiries or assistance, please feel free to reach out to us via email:
                                    <Link to="mailto:HotStrings@mail.com">
                                      <span className="text-warning"> HotStrings@mail.com</span>
                                    </Link>
                                  </p>
                                  <p>You can also use the contact form on our website for a quick response. <Link to="https://mdorio-capstone3.vercel.app/contact"><span className="text-warning">Contact Form</span></Link></p>
                                  <p>We look forward to hearing from you!</p>
                                </div>
                                )}
                                </ul>
                                </Modal.Body>

                                <Modal.Footer className='bg-dark'>
                                  <Button variant="warning" onClick={closeEdit}>
                                    Close
                                  </Button>

                                </Modal.Footer>
                                </Modal>
                            
                            </Row>
                          </Container>
                        </div>
                      </div>
                    ))}
                </div>
              </Col>
            </Row>
          </Container>
        )}
      </div>
    );
    
}    