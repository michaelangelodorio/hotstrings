import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import UpdateRole from '../admin/UpdateRole';
import UserActivation from '../admin/UserActivation';
import { Card, ListGroup } from 'react-bootstrap';

export default function UserView({ users, fetchUserData }) {
  const [user, setUsers] = useState([]);
  const [message, setMessage] = useState('');
  
  useEffect(() => {
    const usersArr = users.map(user => {
        
    if (user.userImg === "")
    {user.userImg="https://cdn.dribbble.com/users/9685/screenshots/997495/avatarzzz.gif"}


      return (
        <Row className="mb-3 vh-auto my-5" key={user._id}>

        <Col className="d-flex col-md-3 blur-content-dark text-center align-items-center">
            <div className='mx-auto'>
                <img className='imgthumbnail' src={`${user.userImg}`} alt="User Image" />
            </div>
        </Col>

        <Col className="col-md-6">
          <Card className='bg-dark'>
            <Card.Body>
              <ListGroup >
                <ListGroup.Item><strong>ID:</strong> <span className='text-dark'>{user._id}</span></ListGroup.Item>
                <ListGroup.Item><strong>Name:</strong> <span className='text-dark'>{`${user.lastName} ${user.firstName}`}</span></ListGroup.Item>
                <ListGroup.Item><strong>Mobile:</strong> <span className='text-dark'>{user.mobileNo}</span></ListGroup.Item>
                <ListGroup.Item><strong>Registered:</strong> {`${user.registerdOn}`}</ListGroup.Item>
                <ListGroup.Item><strong>Image:</strong> <span className='text-dark'><a className='text-info' target='_blank' href={user.userImg}>Image Link</a></span></ListGroup.Item>
                <ListGroup.Item><strong>Address:</strong> <span className='text-dark'>{user.address}</span></ListGroup.Item>
              </ListGroup>
            </Card.Body>
          </Card>
        </Col>
      

        <Col className="d-flex col-md-3 bg-dark text-center align-items-center selected-status">
        
        <Container>
        <Row>
        <Col>
            <Card className={user.isAdmin ? "bg-warning" : "bg-light"}>
                <Card.Body>
                <ListGroup variant="flush">
                    <ListGroup.Item className='bg-dark text-warning text-start'><strong>Status:</strong> <span className={user.isActivated ? "text-success" : "text-danger"}>{user.isActivated ? "Active" : "Deactivated"}</span></ListGroup.Item>
                    <ListGroup.Item className='bg-dark text-warning text-start'><strong>Role:</strong> <span className={user.isAdmin ? "text-warning" : "text-danger"}>{user.isAdmin ? "Admin" : "User"}</span></ListGroup.Item>
                </ListGroup>
                <ListGroup variant="flush">
                    <ListGroup.Item>
                    <div className="d-grid gap-3 ">
                        <UserActivation users={user._id} fetchUserData={fetchUserData} isActivated={user.isActivated} />
                        <UpdateRole users={user._id} fetchUserData={fetchUserData} isAdmin={user.isAdmin} />
                    </div>
                    </ListGroup.Item>
                </ListGroup>
            </Card.Body>
        </Card>
        </Col>
        </Row>
        </Container>
            
        </Col>

      </Row>
      )
    });
  
    setUsers(usersArr);
  }, [users]);
  
  return (
    <>
      <Container >
        <Row>
          <Col>
            {user}
          </Col>
        </Row>
      </Container>
    </>
  )
}
