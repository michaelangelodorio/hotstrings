import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';
import AppNavbar from '../global/AppNavbar';

export default function RemoveFromCart({cart, productId, fetchCartData, isActive}){

	
	const removeItem = (e, productId)=>{

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/carts/removeItem/${productId}`,{

			method:'DELETE',
			headers:{
				'Content-Type':'application/json',
				'Authorization':`Bearer ${localStorage.getItem('token')}`
			}
			
		})
		.then(res=>res.json())
		.then(data=>{

			// {console.log(`Product ID: `)}

			if (data) {
				Swal.fire({
				  title: `Cart`,
				  icon: 'info',
				  text: 'Item removed from the cart!',
				});
				fetchCartData();
			  } else {
				Swal.fire({
				  title: 'Error!',
				  icon: 'error',
				  text: 'Please try again',
				});
				fetchCartData();
			  }
		})

		{console.log(`${productId} ${isActive}`)}

	}

	return (
		<>
		<div className="removeItemButton">
			<span className="position-absolute top-0 start-10 translate-middle">
				<Link className='' size="sm" onClick={e=>removeItem(e,productId)}><i title="remove from cart" className="fa-solid fa-circle-xmark"></i></Link>
			</span>
		</div>
		</>
	)
}