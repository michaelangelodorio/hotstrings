import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';
import AppNavbar from '../global/AppNavbar';

export default function EditUserCart({cart, productId, fetchCartData, isActive}){

	const moveToLikes = (e, productId)=>{

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/carts/${productId}/archive`,{

			method:'PUT',
			headers:{
				'Content-Type':'application/json',
				'Authorization':`Bearer ${localStorage.getItem('token')}`
			}
			
		})
		.then(res=>res.json())
		.then(data=>{

			{console.log(`Product ID: ${productId}`)}

			if(data === true){
				Swal.fire({
					title:'Success!',
					icon:'success',
					text:'Product Moved to Likes ❤️ Successfully!'
				})
				
                fetchCartData();
			}else{
				Swal.fire({
					title:'Error!',
					icon:'error',
					text:'Please try again'
				})
				
                fetchCartData();
			}
		})

		{console.log(`${productId} ${isActive}`)}

	}

	
	const removeItem = (e, productId)=>{

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/carts/removeItem/${productId}`,{

			method:'DELETE',
			headers:{
				'Content-Type':'application/json',
				'Authorization':`Bearer ${localStorage.getItem('token')}`
			}
			
		})
		.then(res=>res.json())
		.then(data=>{

			{console.log(`Product ID: ${productId}`)}

			if (data) {
				Swal.fire({
				  title: `Cart Item ${cart}`,
				  icon: 'info',
				  text: 'Item removed from the cart!',
				});
				fetchCartData();
			  } else {
				Swal.fire({
				  title: 'Error!',
				  icon: 'error',
				  text: 'Please try again',
				});
				fetchCartData();
			  }
		})

		{console.log(`${productId} ${isActive}`)}

	}


	const moveToOrder = (e, productId)=>{

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/carts/${productId}/activate`,{

			method:'PUT',
			headers:{
				'Content-Type':'application/json',
				'Authorization':`Bearer ${localStorage.getItem('token')}`
			}
			
		})
		.then(res=>res.json())
		.then(data=>{

			{console.log(`Product ID: ${productId}`)}

			if(data === true){
				Swal.fire({
					title:'Success!',
					icon:'success',
					text:'Product Moved to Cart Successfully'
				})
				
                fetchCartData();
			}else{
				Swal.fire({
					title:'Error!',
					icon:'error',
					text:'Please try again'
				})
				
                fetchCartData();
			}
		})

		{console.log(`${productId} ${isActive}`)}

	}

	return (
		<>

		{isActive ? 
			<Link className='btn btn-warning d-block' size="sm" onClick={e=>moveToLikes(e,productId)}>Move to Likes ❤️</Link>
			:
			<Link className='btn btn-info d-block' size="sm" onClick={e=>moveToOrder(e,productId)}>Move to Cart</Link>	            
		}
		</>
	)
}