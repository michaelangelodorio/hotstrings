import React, { useState, useEffect, useContext } from 'react';
import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';
import AppNavbar from '../global/AppNavbar';
import { Button, Container, Row, Col, Card } from 'react-bootstrap';

export default function EditUserOrder({order, orderId, fetchOrderData, isActive}){

	const [submitStatus, setSubmitStatus] = useState("")
	const [payClicked, setPayClicked] = useState(false);
	const [orderClicked, setOrderClicked] = useState(true);
	const [cancelClicked, setCancelClicked] = useState(true);

	const handleOrderClick = () => {
		setOrderClicked(true);
		setCancelClicked(true);
		setPayClicked(true);
	  }

	  const handleCancelClick = () => {
		setOrderClicked(true);
		setCancelClicked(true);
		setPayClicked(true);
	  }

	const handlePayClick = () => {
		setCancelClicked(false);
		setOrderClicked(false);
		setPayClicked(true);
	  }

	  
	const completeOrder = (e, orderId)=>{

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/orders/${orderId}/complete`,{

			method:'PUT',
			headers:{
				'Content-Type':'application/json',
				'Authorization':`Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{

			{console.log(`Order ID: ${orderId}`)}

			if(data === true){
				Swal.fire({
					title: 'Are you sure?',
					text: "You won't be able to revert this!",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, to complete!'
				  }).then((result) => {
					if (result.isConfirmed) {
					  Swal.fire(
						'Complete!',
						'Your have completed your order.',
						'success'
					  )
					  handleOrderClick()
						setSubmitStatus("Order Complete");
					}
				  })
				
                fetchOrderData();
			}else{
				Swal.fire({
					title:'Error!',
					icon:'error',
					text:'Please try again'
				})
				
                fetchOrderData();
			}
		},[order])

		{console.log(`${orderId} ${isActive}`)}

	}

	// {console.log(`${order}`)}
	// {console.log(`${process.env.REACT_APP_API_URL}/order/${orderId}/archive`)}

	const cancelOrder = (e, orderId)=>{

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/orders/${orderId}/cancel`,{

			method:'PUT',
			headers:{
				'Content-Type':'application/json',
				'Authorization':`Bearer ${localStorage.getItem('token')}`
			}
			
		})
		.then(res=>res.json())
		.then(data=>{

			{console.log(`Order ID: ${orderId}`)}

			if(data === true){
				Swal.fire({
					title: 'Are you sure?',
					text: "You won't be able to revert this!",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, cancel now!'
				  }).then((result) => {
					if (result.isConfirmed) {
					  Swal.fire(
						'Cancelled!',
						'Your order has been cancelled.',
						'success'
					  )
					  handleCancelClick()
					  setSubmitStatus("Order Cancelled");
					}
				  })
			
                fetchOrderData();
			}else{
				Swal.fire({
					title:'Error!',
					icon:'error',
					text:'Please try again'
				})
				
                fetchOrderData();
			}
		})

		{console.log(`${orderId} ${isActive}`)}

	}

	const payment = (e, orderId)=>{
		e.preventDefault();
	
			if(orderId){
				Swal.fire({
					title: '<strong>Payment</strong>',
					
					html:
					  'You may scan the <b>QR Code</b> below, ' +
					  '<img width="300px" className="imgthumbnail" src="https://m-dorio.github.io/foodstore/images/qrcode2.png">',
	
					showCloseButton: true,
					showCancelButton: true,
		
					confirmButtonText:
					  '<i class="fa fa-thumbs-up"></i> Great!',
					confirmButtonAriaLabel: 'Thumbs up, great!',
					cancelButtonText:
					  '<i class="fa fa-thumbs-down"></i>',
					cancelButtonAriaLabel: 'Thumbs down'
				  }).then((result) => {
					if (result.isConfirmed) {
						Swal.fire({
							title:'Confirm your payment:',
							text: "Once paid, you will receive a notification or sms.",
							icon: 'info',
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonText: 'Yes, confirmed!'
						  })
						  handlePayClick()
						  setSubmitStatus("QR Scanned.");
					}
				  })

                	fetchOrderData();
			}else{
				Swal.fire({
					title:'Error!',
					icon:'error',
					text:'Please try again'
				})
				
                fetchOrderData();
			}

	}

	return (
		<>

		{isActive ? 
		<>
			{/* <Link className='btn btn-warning' size="sm" onClick={e=>completeOrder(e,orderId)}>Complete Order</Link>
			<Link className='btn btn-info ' size="sm" onClick={e=>cancelOrder(e,orderId)}>Cancel Order</Link>	 */}
			<div className="h4 text-center text-warning">{submitStatus}</div>
		</>
			:
		<>
			<div className="h4 text-center text-warning">{submitStatus}</div>
		</>
		}
 <Row className='d-flex justify-content-evenly align-items-center'>
        <Link
          className={`btn btn-success my-2 ${orderClicked ? 'disabled' : ''}`}
          size="sm"
          onClick={(e) => completeOrder(e, orderId)}
          disabled={orderClicked}
        >
          Complete Order
        </Link>
        <Link
          className={`btn btn-danger my-2 ${cancelClicked ? 'disabled' : ''}`}
          size="sm"
          onClick={(e) => cancelOrder(e, orderId)}
          disabled={cancelClicked}
        >
          Cancel Order
        </Link>
        <Link
          className={`btn btn-primary my-2 ${payClicked ? 'disabled' : ''}`}
          title='Pay Now'
          size="sm"
          onClick={(e) => payment(e, orderId)}
		  disabled={payClicked}
        >
          Pay Now
        </Link>
      </Row>

	</>
	)
}