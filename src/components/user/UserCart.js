import React, { useState, useEffect, useContext } from 'react';
import { Card, Container, Row, Col, Tab, Tabs, Button, Table} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import FormatCurrency from '../global/FormatCurrency';
// import FormatDate from '../global/FormatDate';
import EditUserCart from './EditUserCart'
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';
import AppNavbar from '../global/AppNavbar';
import RemoveFromCart from './RemoveFromCart';

export default function UserCart({ productId, status, fetchCartData, cart }) {

    const [cartCount, setCartCount] = useState(0); // Initialize cartCount with 0
    const [likesCount, setLikesCount] = useState(0); // Initialize likesCount with 0
    const {user, setUser} = useContext(UserContext);
    const [myCart, setCart] = useState([]);

    useEffect(() => {
      // fetchCartData();
      setCart(cart);
      const activeItems = myCart.filter(item => item.isActive && !item.isOrdered).length;
      const inactiveItems = myCart.filter(item => !item.isActive && !item.isOrdered).length;
      setCartCount(activeItems)
      setLikesCount(inactiveItems)
  }, [fetchCartData]);

  const clearCart = (e)=>{
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/carts/clear`,{

			method:'PUT',
			headers:{
				'Content-Type':'application/json',
				'Authorization':`Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{

			if(data){
				Swal.fire({
					title:'Info!',
					icon:'info',
					text:'Cart is now Empty.'
				})
				
        setCart([]);
        setCartCount(0);
        setLikesCount(0);
			}else{
				Swal.fire({
					title:'Error!',
					icon:'error',
					text:'Please try again'
				})
			}

		})
	}

  const checkout = (e) => {
    e.preventDefault();
  
    fetch(`${process.env.REACT_APP_API_URL}/carts/checkout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((res) => {
        if (!res.ok) {
          throw new Error('Cart is empty at the moment.');
        }
       
        return res.json();
      })
      .then((data) => {
        if (data) {
          Swal.fire({
            title: 'Success!',
            icon: 'success',
            text: 'Thank you! Checkout successfully!',
          });
  
          // // Filter out products with isActive set to false
          // const activeProducts = data.filter(product => product.isActive === true);
          
          // setCart(activeProducts);
          // setCartCount(0);
          // setLikesCount(0);
  
        } else {
          Swal.fire({
            title: 'Error!',
            icon: 'error',
            text: 'Please try again',
          });
        }
      })
      .catch((error) => {
        console.info('Error during checkout:', error);
        Swal.fire({
          title: 'Info!',
          icon: 'info',
          text: 'Please add some items and try again.',
        });
      });
  };
  
  
  return (

    <>
     <Container id="userview" className=' my-5'>
     <Row>
     <Col xs={12} sm={12} md={12} lg={12} xl={12} className="m-3 text-center">
              <h2 className='page-header text-white'>My Cart</h2>
              <Button title='checkout' className='my-2 btn btn-success mx-3' size="sm" onClick={e => checkout(e, productId)}>Checkout&nbsp;<i className="fa-solid fa-cash-register"></i></Button>
            <Button title='clear' className='my-2 btn btn-danger mx-3' size="sm" onClick={e => clearCart(e, productId)}>Clear&nbsp;Cart&nbsp;<i className="fa-solid fa-eraser"></i></Button>
        </Col>
    </Row>


        <Row>
          <Col id="tabs">
            <Tabs id="fill-tab-example" className="mb-3" fill>

              <Tab id="_Featured" eventKey="featured" title={`In the Cart (${cartCount})`}>

              <div className="d-flex flex-wrap justify-content-center ">
                  {myCart.reduce((uniqueProducts, product) => {
                    // Check if the product already exists in uniqueProducts array
                    const existingProduct = uniqueProducts.find((p) => p._id === product._id);

                    if (existingProduct) {
                      // If the product exists, increment its count
                      existingProduct.count += 1;
                    } else {
                      // If it doesn't exist, add it to the uniqueProducts array
                      uniqueProducts.push({ ...product, count: 1 });
                    }
                    // const newProduct = uniqueProducts.find((p) => product.isActive === true)
                    return uniqueProducts; // Return the updated uniqueProducts array

                  }, [myCart]).filter((product) => product.count > 0 && product.isActive === true && !product.isOrdered).map((product) => (
                    
                      <Col key={product._id} xs={10} sm={10} md={5} lg={4} xl={3} className="m-3 text-white">
                        
                      <Card className='bg-dark text-white productHighlight '>
                      <RemoveFromCart cart={cart} productId={product._id} fetchCartData={fetchCartData} isActive={product.isActive} />
                        <Card.Header>
                          {/* <Card.Text> <li>Item: {product._id}</li></Card.Text> */}
                          <Card.Text> <li>Item: {product.name}</li></Card.Text>
                        </Card.Header>
                        <Card.Body>
                          <Container>
                          <Col>
                              <Link to={`/products/${product.productId}`}>
                              <Card.Img variant="top" className='img-fluid border rounded mb-2' src={product.productImg} alt={`Product ${product.productId}`} />
                              </Link>
                              </Col>
                            <Row>
                              <Col className="cart-product-content py-2 text-white">
                                  <ul>
                                    <li><span>Price: {FormatCurrency(product.price)}</span></li>
                                    <li><span>Subtotal: {FormatCurrency(product.subtotal)}</span></li>
                                    {/* <li>User: {product.userId}</li> */}
                                    {/* <li>{`${product.isActive}`}</li> */}
                                    <li><span>Quantity: {product.quantity}</span></li>
                                  </ul>
                              </Col>
                              </Row>
                              <Row>
                              <Col><Card.Text><li>Added On: {product.addedOn}</li></Card.Text></Col>
                            </Row>
                          </Container>
                        </Card.Body>
                        <Card.Footer>
                          <EditUserCart cart={cart} productId={product._id} fetchCartData={fetchCartData} isActive={product.isActive} />
                        </Card.Footer>
                      </Card>
                    </Col>
                  ))}
                </div>
              </Tab>

              <Tab id="Myfavs" eventKey="likes" title={`My Likes ❤️ (${likesCount})`}>
              <div className="d-flex flex-wrap justify-content-center">        
              {myCart
              .reduce((uniqueProducts, product) => {
                    // Check if the product already exists in uniqueProducts array
                    const existingProduct = uniqueProducts.find((p) =>
                     p._id === product._id);

                    if (existingProduct) {
                    // If the product exists, increment its count
                    existingProduct.count += 1;
                    } else {
                    // If it doesn't exist, add it to the uniqueProducts array
                    uniqueProducts.push({ ...product, count: 1 });
                    }

                    // const newProduct = uniqueProducts.find((p) => product.isActive === false)
                    
                    return (uniqueProducts);

                    }, [myCart]).filter((product)=> product.count > 0 && product.isActive === false && !product.isOrdered).map((product) => (
                     
                    <Col key={product._id} xs={10} sm={10} md={5} lg={4} xl={3} className="m-3 text-white">
                      
                     <Card className='bg-dark text-white productHighlight mx-1'>
                     <RemoveFromCart cart={cart} productId={product._id} fetchCartData={fetchCartData} isActive={product.isActive} />
                     <Card.Header>
                          {/* <Card.Text> <li>Item: {product._id}</li></Card.Text> */}
                          <Card.Text> <li>Item: {product.name}</li></Card.Text>
                        </Card.Header>
                        <Card.Body>
                          <Container>
                            <Row>
                              <Col>
                              <Link to={`/products/${product.productId}`}>
                              <Card.Img variant="top" className='img-fluid border rounded mb-2' src={product.productImg} alt={`Product ${product.productId}`} />
                              </Link>
                              </Col>
                            </Row>
                            <Row>
                              <Col className="cart-product-content py-2 text-white">
                                  <ul>
                                    <li><span>Price: {FormatCurrency(product.price)}</span></li>
                                    <li><span>Subtotal: {FormatCurrency(product.subtotal)}</span></li>
                                    {/* <li>User: {product.userId}</li> */}
                                    {/* <li>{`${product.isActive}`}</li> */}
                                    <li><span>Quantity: {product.quantity}</span></li>
                                  </ul>
                              </Col>
                              </Row>
                              <Row>
                              <Col><Card.Text><li>Added On: {product.addedOn}</li></Card.Text></Col>
                            </Row>
                          </Container>
                        </Card.Body>

                     <Card.Footer>
                     <EditUserCart productId={product._id} fetchCartData={fetchCartData} isActive={product.isActive}/>
                     </Card.Footer>
                     </Card>
                     </Col>
                 ))}       
                </div>
              </Tab>

            </Tabs>
          </Col>
        </Row>
      </Container>

    </>

  );
}
