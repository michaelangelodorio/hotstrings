import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { DropdownButton, Dropdown, Button } from 'react-bootstrap';

export default function ColorPicker ({ selectedColor , handleColorChange }) {

  const colorOptions = [
    'white',
    '#43464B',
    'lightblue',
    'lightgreen',
    'lightpink',
    'lightcoral',
    'lightgoldenrodyellow',
    'lightyellow',
    'lightcyan',
    '#475908',
    '#8BA630',
    'lightsteelblue',
    'lightseagreen',
    'lightcoral',
    'lightgoldenrodyellow',
    'lightslategray',
    'violet',
    'gray',
    '#B946F2',
    '#8834B3'
  ];

  return (
    <div>
      <div className='d-flex'>
        <DropdownButton id="dropdown-basic-button" title="Color">
          {colorOptions.map((color, index) => (
            <Dropdown.Item
              key={index}
              style={{ backgroundColor: color }}
              onClick={() => handleColorChange(color)}
            >
              {color}
            </Dropdown.Item>
          ))}
        </DropdownButton>

      </div>
    </div>
  );
};
