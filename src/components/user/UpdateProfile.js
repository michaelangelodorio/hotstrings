import React, { useState, useEffect } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

function UpdateProfile({user, fetchData}) {
  const [userData, setUserData] = useState({
    userId: '',
    firstName: '',
    lastName: '',
    age: '',
    // email: '',
    address: '',
    mobileNo: '',
    userImg: '',
  });
  const [showEdit, setShowEdit] = useState(false);

  useEffect(() => {
    if (showEdit) {
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
        .then((res) => res.json())
        .then((data) => {
          setUserData(data);
        });

    }

  }, [showEdit]);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setUserData({
      ...userData,
      [name]: value
    });
  };

  const handleUpdateProfile = async () => {
    const apiUrl = `${process.env.REACT_APP_API_URL}/users/profile/${user}`;

    try {
      const response = await fetch(apiUrl, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
          firstName: userData.firstName,
          lastName: userData.lastName,
          age: userData.age,
          mobileNo: userData.mobileNo,
          address: userData.address,
          email: userData.email,
          userImg: userData.userImg,
        })
      });


      if (response.ok) {
        Swal.fire({
          title: 'Success!',
          icon: 'success',
          text: 'Profile Successfully Updated'
        });
        closeEdit();
      } else {
        Swal.fire({
          title: 'Error!',
          icon: 'error',
          text: 'Please try again'
        });
        closeEdit();
      }
    } catch (error) {
      console.error('Error:', error);

    }
  };

  const openEdit = () => {
    // setMessage('');
    setShowEdit(true);
  };

  const closeEdit = () => {
    // setMessage('');
    setShowEdit(false);
  };

  return (
    <>
    <Button title='Edit Profile' variant="secondary" size="sm" onClick={openEdit}><i className="fa-solid fa-gear fa-fade"></i></Button>
      <Modal show={showEdit} onHide={closeEdit}>
        <Form>
          <Modal.Header closeButton>
            <Modal.Title>Edit User</Modal.Title>
          </Modal.Header>

          <Modal.Body>
          <Form.Label className='bg-dark p-2 text-warning'>{`User Email: ${userData.email}`}</Form.Label>
          <Form.Group controlId="firstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                name="firstName"
                value={userData.firstName}
                onChange={handleInputChange}
                required
              />
            </Form.Group>

            <Form.Group controlId="lastName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text"
                name="lastName"
                value={userData.lastName}
                onChange={handleInputChange}
                required
              />
            </Form.Group>

            <Form.Group controlId="age">
              <Form.Label>Age</Form.Label>
              <Form.Control
                type="text"
                name="age"
                value={userData.age}
                onChange={handleInputChange}
                required
              />
            </Form.Group>

            <Form.Group controlId="mobileNo">
              <Form.Label>Mobile No</Form.Label>
              <Form.Control
                type="text"
                name="mobileNo"
                value={userData.mobileNo}
                onChange={handleInputChange}
                required
              />
            </Form.Group>

            <Form.Group controlId="address">
              <Form.Label>Address</Form.Label>
              <Form.Control
                type="text"
                name="address"
                value={userData.address}
                onChange={handleInputChange}
                required
              />
            </Form.Group>

            <Form.Group controlId="userImg">
              <Form.Label>User Image</Form.Label>
              <Form.Control
                type="text"
                name="userImg"
                value={userData.userImg}
                onChange={handleInputChange}
                required
              />
            </Form.Group>
    
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={closeEdit}>
              Close
            </Button>
            <Button variant="success" onClick={handleUpdateProfile}>
              Update Profile
            </Button>
          </Modal.Footer>
          
        </Form>
      </Modal>
    </>
  );
}

export default UpdateProfile;
