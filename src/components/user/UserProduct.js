import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Tab, Tabs, Table } from 'react-bootstrap';
import ProductSearch from '../product/ProductSearch';
import SearchByPrice from '../product/SearchByPrice'
import AllProducts from '../admin/AllProducts'
import FeaturedProducts from '../product/FeaturedProducts';

export default function UserProduct({user, products, fetchProductData}) {
  
  const [product, setProduct] = useState([]);

  useEffect(() => {
    // fetchProductData();
    setProduct(products);
}, [fetchProductData]);


  return (
    <>
    
    <div id="userview">
        <Container fluid>

        <Row>
          <Col id="tabs" className='vh-auto py-5 my-5'>
          {user._id !== '' ? (
            <Tabs
              defaultActiveKey="name"
              id="fill-tab-example"
              className="mb-5"
              fill
            >
              <Tab eventKey="name" title="Search By Name">
              <ProductSearch />
              </Tab>

              <Tab eventKey="featured" title="All">
              <AllProducts user={user} products={products} fetchProductData={fetchProductData}  />
              </Tab>
           
              <Tab eventKey="price" title="Search By Price">
                <SearchByPrice />
              </Tab>

            </Tabs>):(
              <>
                <FeaturedProducts />
                <SearchByPrice />
                </>
            )}
          </Col>
        </Row>
      </Container>

  </div>
</>);
}
