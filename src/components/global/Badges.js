import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext';

export default function Badges({ data, fetchData }) {
  const { user } = useContext(UserContext);
  const [count, setCount] = useState(0);

//   const fetchData = () => {
//     fetch(`${process.env.REACT_APP_API_URL}/cart/`, {
//       headers: {
//         'Content-Type': 'application/json',
//         Authorization: `Bearer ${localStorage.getItem('token')}`
//       }
//     })
//       .then((res) => {
//         if (!res.ok) {
//           throw new Error('Network response was not ok');
//         }
//         return res.json();
//       })
//       .then((data) => {
//         setCount(count);
//       })
//       .catch((error) => {
//         setError('Error fetching all product data. Please try again later.');
//       });
//   }

//   useEffect(() => {
//     fetchData();

//   }, []);

//   console.log("#: ", count);

  return (
    <div className="badge">
      {count > 0 && (
        <span className="position-absolute top-0 start-10 translate-middle badge rounded-pill bg-danger">
          {count}
          <span className="visually-hidden">unread messages</span>
        </span>
      )}
    </div>
  );
}
