import React, { useContext, useState, useEffect } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Container, Nav, Navbar } from 'react-bootstrap';
import UserContext from '../../UserContext';
import Badges from './Badges';

export default function AppNavbar({cart,fetchCartData, order, fetchOrderData}) {

  const { user } = useContext(UserContext);

  // useEffect(() => {
  //   fetchCartData();
  //   fetchOrderData();
  // }, []); 

  try {
    return (

      <Navbar fixed='top' bg="dark" data-bs-theme="dark" expand="lg">
            <Container fluid>
                <Navbar.Brand as={Link} to="/">
                
                  <span className="textfx">H</span><span className="logo">/S&nbsp;<i className="bowl-rice fa-solid fa-bowl-rice"></i></span>
                  </Navbar.Brand>

                  <Nav.Link>
                    <span className="d-flex logo-container">
                    <i className="d-block d-xs-block d-sm-block d-md-none fa-solid fa-mobile-screen-button"></i>
                    <i className="d-none d-md-block d-lg-none fa-solid fa-tablet-screen-button"></i>
                    <i className="d-none d-sm-none d-md-none d-lg-block d-xl-none fa-solid fa-laptop"></i>
                    <i className="d-none d-md-none d-lg-none d-xl-block fa-solid fa-desktop"></i>
                    </span>
                    </Nav.Link>

                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto align-items-center">
                      
                    {user.id !== null ? (
                      user.isAdmin ? (
                          <>
                        <Nav.Link className='mx-2' as={ NavLink } to="/products" exact="true">Manage Products</Nav.Link>
                        <Nav.Link className='mx-2' as={ NavLink } to="/orders">Manage Orders</Nav.Link>
                        <Nav.Link className='mx-2' as={ NavLink } to="/users" exact="true">Manage Users</Nav.Link>
                        <Nav.Link className='btn btn-outline-secondary btn-dark px-4 mx-2' as={ NavLink } to="/logout"><span className='text-warning'>Logout </span></Nav.Link>
                          </>
                      ) : (
                          <>
                        <Nav.Link title='Available Products' className='mx-3' as={ NavLink } to="/products" exact="true">Products</Nav.Link>

                          <Nav.Link title='My Cart' className='mx-3 position-relative' as={ NavLink } to="/carts" exact="true">Cart
                          <Badges cart={cart} fetchCartData={fetchCartData} />
                        </Nav.Link>
                        
                        <Nav.Link title='My Orders' className='mx-3 position-relative' as={ NavLink } to="/orders" exact="true">Orders
                            <Badges order={order} fetchOrderData={fetchOrderData} />
                        </Nav.Link>
                        <Nav.Link className='mx-3' as={ NavLink } to="/contact" exact="true">Contact</Nav.Link>
                            
                              </>
                          )
                      ) : (
                        <>
                            <Nav.Link className='mx-3' as={ NavLink } to="/products" exact="true">Products</Nav.Link>
                            <Nav.Link className='mx-3' as={ NavLink } to="/contact" exact="true">Contact</Nav.Link>
                            <Nav.Link className='mx-3' as={ NavLink } to="/users/login" exact="true">Login</Nav.Link>
                            <Nav.Link className='mx-3' as={ NavLink } to="/users/register" exact="true">Register</Nav.Link>
                        </>
                      )}
                  </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
 
    )
  } catch (error) {
    // Handle any potential rendering errors here
    console.error('Error in AppNavbar:', error);
    return null;
  }
}
