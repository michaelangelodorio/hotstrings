import { Link } from 'react-router-dom';
import React, { useEffect, useState } from 'react';

export default function Footer() {

    let replaceDate = () => {
        let modate = document.lastModified;
        const updatedDates = document.querySelectorAll("#w-update");
    
        for (let i = 0; i < updatedDates.length; i++) {
            modate = document.querySelectorAll("#w-update").innerHTML = "Updated last: " + new Date(modate).toString();
            updatedDates[i].innerHTML = modate;
        }
    }
    replaceDate();


     
    return(

        //WEB
        <>
        <footer id="footer">
        <div className="d-none d-md-none d-lg-block px-3 d-flex align-items-center justify-content-center">
            <div className="text-white">
                Copyright &copy; 2023 by <Link className="icon-glow" href="https://m-dorio.github.io/webportfolio/">mdorio.</Link> | All rights Reserved.
                </div>   
            <div className="text-white">
                <small id="w-update" className="text-white"></small>
            </div>
        </div>  
        <div className="d-block md-block d-lg-none d-flex align-items-center justify-content-center">
            <div className="text-white">
                Copyright &copy; 2023 by <Link className="icon-glow" href="https://m-dorio.github.io/webportfolio/">mdorio.</Link> | All rights Reserved.
                </div>   
            <div className="text-white">
                <small id="w-update" className="text-white"></small>
            </div>
        </div>  

        </footer>

               </>
    )
}