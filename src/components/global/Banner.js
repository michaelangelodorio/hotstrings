import Login from '../../pages/Login';
import FeaturedProducts from '../product/FeaturedProducts';
import {Container, Row, Col} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css'
import { Link } from 'react-router-dom';

export default function Banner() {

	return (
	<>
	<section >
	
		<Container fluid>
		
			<Row className="justify-content-center align-items-center">
			<Col xs="12" md="auto" className="text-center">
			<div className="d-block d-sm-block d-md-none ">
				<p title='Back to home' className="banner textfxa">
				HOT
				<a className="mt-5 textfxb" href="./">
					/STRINGS
				</a>
				</p>
			</div>
		
			<div className="d-none d-sm-none d-md-block">
				<p title='Back to home' className="banner textfxa">
				HOT
				<a className="mt-5 textfxb" href="./">
					/STRINGS&nbsp;<i className="bowl-rice fa-solid fa-bowl-rice"></i>
				</a>
				</p>
			</div>
			<FeaturedProducts />
			</Col>
			</Row>
		</Container>

	</section>
	</>
	
	)
}


