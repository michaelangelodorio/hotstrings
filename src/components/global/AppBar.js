import React, { useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../../UserContext';
import { Container, Navbar, Button } from 'react-bootstrap';

export default function BottomAppBar() {
  const { user } = useContext(UserContext);

  try {
    return (
      <>
      <Navbar  id="bottom" fixed='bottom' className="bg-dark">
        <Container fluid>

        {user.id !== null ? (
        user.isAdmin ? (
        <>
        <Link  title='Logout' className="btn btn-dark px-4 mx-3 btn-outline-light position-relative ms-auto d-flex align-items-center" as={NavLink} to="/logout">
        <i className="fa-solid fa-right-from-bracket me-2"></i>
        </Link>
          
        </>
        ) : (
        <>
        <div className='d-flex ms-auto'>

            {/* Profile */}
            <Link title='Profile' className="ms-auto btn btn-outline-light position-relative" as={NavLink} to="/users/profile">
            <i className="fa-solid fa-circle-user"></i>
            </Link>

            {/* Logout */}
            <Link title='Logout' className="mx-3 btn btn-outline-light position-relative" as={NavLink} to="/logout">
            <i className="fa-solid fa-right-from-bracket me-2"></i>
            </Link>
        </div>
        </>
        )
    ) : (
        <>
        <div className="justify-content-between">

        </div>
        </>
    )}
        </Container>
      </Navbar>
      </>
    );
  } catch (error) {
    // Handle any errors here
    console.error(error);
    return null; // Return null or a fallback component in case of an error
  }
}
