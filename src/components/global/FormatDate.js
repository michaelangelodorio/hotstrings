import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

export default function FormatDate(date) {
    return new Intl.DateTimeFormat('en-US', 
    { 
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric',
        timeZoneName: 'short'
    }).format(date);
};

