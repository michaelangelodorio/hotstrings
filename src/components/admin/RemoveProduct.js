
import { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function RemoveProduct({products, fetchProductData, isActive}){

	const removeProduct = (e, products)=>{

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/${products}/remove`,{

			method:'DELETE',
			headers:{
				'Content-Type':'application/json',
				'Authorization':`Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)

			if(data){
				Swal.fire({
					title:'Oh No!',
					icon:'info',
					text:'Product Removed Successfully.'
				})

                fetchProductData();
			}else{
				Swal.fire({
					title:'Error!',
					icon:'error',
					text:'Please try again'
				})
			
                fetchProductData();
			}
		})
	}

	console.log(products)

	return (
		<>
			<Button variant="danger" size="sm" onClick={e=>removeProduct(e,products)}><i className="fa-solid fa-trash-can"></i></Button>
		</>


	)
}