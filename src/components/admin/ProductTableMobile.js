import React, { useState, useEffect, useContext } from 'react';
import { Accordion, Container, Row, Col, Table } from 'react-bootstrap';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';
import RemoveProduct from './RemoveProduct';
import UserContext from "../../UserContext";
import formatDate from '../../components/global/FormatDate';

export default function ProductTableMobile({ fetchProductData, products, endpoint }) {

    const [prods, setProducts] = useState([]);
    const { user } = useContext(UserContext);

    useEffect(() => {
  
        const productsArr = products.map(product => {
        return (
        <Row key={product._id}>
            <Col className="bg-dark inventory-item">
                <Container>
                    <Col className="item-info">
                        <span className="text-white item-id ellipsis">ID: {product._id}</span>
                    </Col>
                    <Col className="item-info">
                        <span className="text-white item-name">{product.name}</span>
                    </Col>
                    <Col className="item-details">
                        <Col className="text-white  item-image">
                            <img src={`${product.productImg}`} alt="Product Image" />
                        </Col>
                    </Col>
                    
                    <Col><span className="item-description text-secondary ">{product.description}</span></Col>
                    <Col>
                    <li className='text-start text-warning'><strong>ID:</strong><span className='text-light'>{product._id}</span></li>
                    <li className='text-start text-warning'><strong>Name:</strong><span className='text-light'>{product.name}</span></li>
                    <li className='text-start text-warning'><strong>Description:</strong><span className='text-light'>{product.description}</span></li>
                    <li className='text-start text-info'><strong>Average Ratings: </strong><span className='h4 text-warning'>
                        {Array.from({ length: product.averageRating }, (_, index) => (<span key={index}>&#9733;</span>))}</span></li>
                    </Col>
                                    <Col>
                 
                                    <strong>Ratings:</strong>
                                    <Accordion defaultActiveKey={['0']}>
                                        <Accordion.Item eventKey="1">
                                            <Accordion.Header>Comments/Ratings</Accordion.Header>
                                            <Accordion.Body className='bg-dark'>
                                                {product.ratings.map((rating, index) => (
                                                    <div className="p-2 my-2 border border-dark-subtle" key={index}>
                                                        <Accordion>
                                                            <Accordion.Item eventKey={`item-${index}`}>
                                                                <Accordion.Header>User: {rating.userName}</Accordion.Header>
                                                                <Accordion.Body className='bg-dark text-white'>
                                                                    {/* {rating.userImg === "" ? 
                                                                        (<img id="img-thumbnail profile_img" className='d-flex align-content-center' src="https://cdn.dribbble.com/users/9685/screenshots/997495/avatarzzz.gif" alt="default avatar" />)
                                                                        :
                                                                        (<img id="img-thumbnail profile_img" className='d-flex align-content-center' src={rating.userImg} alt={`avatar of ${rating.userName}`} />)
                                                                    } */}
                                                                    <div>
                                                                        <span className="text-warning">Date: </span>
                                                                        <span className="text-white">{formatDate(rating.date)}</span>
                                                                    </div>
                                                                    <div>
                                                                        <span className="text-warning">Comment: </span>
                                                                        <span className="text-white">{`"${rating.comment}"`}</span>
                                                                    </div>
                                                                    <div>
                                                                        <span className="text-warning">Rate: </span>
                                                                        <span className="text-white">{rating.rate}</span>
                                                                    </div>
                                                                </Accordion.Body>
                                                            </Accordion.Item>
                                                        </Accordion>
                                                    </div>
                                                ))}
                                            </Accordion.Body>
                                        </Accordion.Item>
                                    </Accordion>
                            
                    </Col>
                    <Col className="item-info">
                        <span className="item-price text-white">Price: {product.price}</span>
                        <span className="item-quantity text-white">Quantity: {product.quantity}</span>
                    </Col>
                    <Col className="availability py-1">
                    <span className="text-white">
                        Status:</span> <span className={product.isActive ? "text-info" : "text-danger"}>{product.isActive ? 'Available' : 'Unavailable'}</span>
                    </Col>
                    <Col  className="mt-2 gap-4 item-actions">
                        <ArchiveProduct endpoint={endpoint} product={product._id} fetchProductData={fetchProductData} isActive={product.isActive}/> 
                        <EditProduct product={product._id} fetchProductData={fetchProductData}/>
                        <RemoveProduct endpoint={endpoint} product={product._id} fetchProductData={fetchProductData}/>
                    </Col>
                 
                </Container>
            </Col>
        </Row>
            )
        });


    setProducts(productsArr);
    }, [products]);


    return (
    <>

    {prods}
                  

    </>
     
    );
}