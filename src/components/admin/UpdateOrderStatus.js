import React, { useState, useEffect } from 'react';
import { Button, Modal, Form, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function UpdateOrderStatus({ fetchOrderData, order}) {

        const [currentOrderStatus, setOrderStatus] = useState('Ordered');
        const [currentColorStatus, setColorStatus] = useState('danger');
        const [userActions, setUserActions] = useState('Pending Payment');

        const statusOptions = [userActions,'Ordered', 'Processing', 'Preparing', 'Packing', 'For Delivery', 'In Transit', 'Delivered'];
        const colorStatus = ['danger','light', 'primary', 'secondary', 'light', 'info', 'warning', 'success'];
    
        const currentIndex = statusOptions.indexOf(currentOrderStatus);
        const nextIndex = (currentIndex + 1) % statusOptions.length;
        const nextStatus = statusOptions[nextIndex];
    
        useEffect(() => {
            const fetchOrderStatus = async () => {
                try {
                    const response = await fetch(`${process.env.REACT_APP_API_URL}/orders/${order}/statusUpdate`, {
                        method: 'GET',
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${localStorage.getItem('token')}`
                        }
                    });
                    if (!response.ok) {
                        throw new Error(`Failed to fetch order status (${response.status})`);
                    }
                    const data = await response.json();
                    setOrderStatus(data.orderStatus);
                    setColorStatus(colorStatus[statusOptions.indexOf(data.orderStatus)]);
                    setUserActions(data.userActions);
                    console.log(data);
                } catch (error) {
                    console.error('Error:', error.message);
                }
            };
    
            fetchOrderStatus();
        }, [order]);
    
        const statusToggle = async (e) => {
            e.preventDefault();
    
            try {
                const response = await fetch(`${process.env.REACT_APP_API_URL}/orders/${order}/statusUpdate`, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({ orderStatus: nextStatus })
                });
    
                if (!response.ok) {
                    throw new Error(`Failed to update order status (${response.status})`);
                }
    
                const data = await response.json();
                setOrderStatus(data.orderStatus);
                setColorStatus(colorStatus[statusOptions.indexOf(data.orderStatus)]);
                setUserActions(data.userActions);
                fetchOrderData();
                console.log(`Updated order status to: ${data.orderStatus}`);
            } catch (error) {
                console.error('Error:', error.message);
            }
        };
    
    
        return (
            <div>
                <Col className="mt-2 align-content-center">
                    {currentColorStatus === undefined ? 
                        setColorStatus(colorStatus[0])
                    :
                    <button className={`w-100 btn btn-${currentColorStatus}`} onClick={statusToggle}>
                        <i className="fa-solid fa-circle-left fa-fade"></i>&nbsp;{currentOrderStatus}
                    </button>
                     }
                </Col>
            </div>
        );

};
