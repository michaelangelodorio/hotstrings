import React, { useState, useEffect, useContext } from "react";
import { Accordion, Col, Container, Row, Table } from 'react-bootstrap';
import EditProduct from '../admin/EditProduct';
import ArchiveProduct from '../admin/ArchiveProduct';
import RemoveProduct from './RemoveProduct';
import UserContext from "../../UserContext";
import FormatDate from "../global/FormatDate";

export default function ProductTable({ fetchProductData, products }) {

    const [prods, setProducts] = useState([]);
    const { user } = useContext(UserContext);

    useEffect(() => {
  
const productsArr = products.map(product => {
return (
    <tr key={product._id}>
        <td>
            <Table hover responsive size="sm" variant="dark">
            <tbody>
                <tr>  
                <td>
                <div>
                    <div><span className='text-light'><img className='imgthumbnail' src={`${product.productImg}`}/></span></div>
                </div>
                </td>

                <td className="full-td-width">
                <div className="d-flex align-content-start mx-1">
                    <div>
                    <li className='text-start'><strong>ID:</strong><span className='text-light'>{product._id}</span></li>
                    <li className='text-start'><strong>Name:</strong><span className='text-light'>{product.name}</span></li>
                    <li className='text-start'><strong>Description:</strong> <span className='text-light'>{product.description}</span></li>
                    <li className='text-start'><strong>Average Ratings: {product.averageRating.toFixed(2)}</strong></li>
                    <li className='text-start'><strong>Price:</strong> {product.price}</li>
                    <li className='text-start'><strong>Quantity:</strong> {product.quantity}</li>
                    </div>
                </div>
                </td>
                <td>
                <Accordion defaultActiveKey={['0']}>
                            <Accordion.Item eventKey="1">
                                <Accordion.Header>Comments/Ratings</Accordion.Header>
                                <Accordion.Body className='bg-dark'>
                                    {product.ratings.map((rating, index) => (
                                        <div className="p-2 my-2 border border-dark-subtle" key={index}>
                                            <Accordion>
                                                <Accordion.Item eventKey={`item-${index}`}>
                                                    <Accordion.Header>User: {rating.userName}</Accordion.Header>
                                                    <Accordion.Body className='bg-dark text-white'>                                   <div>
                                                            <span className="text-warning">Date: </span>
                                                            <span className="text-white">{FormatDate(rating.date)}</span>
                                                        </div>
                                                        <div>
                                                            <span className="text-warning">Comment: </span>
                                                            <span className="text-white">{`"${rating.comment}"`}</span>
                                                        </div>
                                                        <div>
                                                            <span className="text-warning">Rate: </span>
                                                            <span className="text-white">{rating.rate}</span>
                                                        </div>
                                                    </Accordion.Body>
                                                </Accordion.Item>
                                            </Accordion>
                                        </div>
                                    ))}
                                </Accordion.Body>
                            </Accordion.Item>
                        </Accordion>
                </td>
                <td>
                    <ul>
                        <li><RemoveProduct products={product._id} fetchProductData={fetchProductData}/></li>
                    </ul>
                </td>

                </tr>
            </tbody>
            </Table>
        </td>
                                    
        <td>
            <Table hover responsive size="sm" variant="dark">
            <tbody>
                <tr>
                <td>
               
                <li><strong>Availability:</strong> 
                <span className={product.isActive ? "text-info" : "text-danger"}>
                    {product.isActive ? "Available" : "Unavailable"}
                </span>
                </li>
                <li className='text-start'><strong>Availability:</strong><span className={product.isActive ? "text-info" : "text-danger"}>{product.isActive ? "Available" : "Unavailable"}</span></li>
                </td>
                </tr>
            </tbody>
            </Table>
        </td>

        <td>
            <Table hover responsive size="sm" variant="dark">
            <tbody>
                <tr>
                <td className="d-grid mt-2 gap-4">
                    <ArchiveProduct products={product._id} fetchProductData={fetchProductData} isActive={product.isActive}/> 
                    <EditProduct products={product._id} fetchProductData={fetchProductData}/>
                </td>
                </tr>
            </tbody>
            </Table>
        </td>
    </tr> 
    )
});

    setProducts(productsArr);
    }, [products]);
       
    return (
    <>
    {prods}          
    </>
    );
}