import React, { useState, useEffect } from 'react';
import { Row, Col, Card, Accordion } from 'react-bootstrap';
import UpdateOrderStatus from './UpdateOrderStatus';
import FormatCurrency from '../global/FormatCurrency';

export default function OrderTable({ fetchOrderData, order}) {

    const [orders, setOrders] = useState([]);

    useEffect(() => {

            const ordersArr = order.map(order => {
            const statusOptions = [order.userActions,'Ordered', 'Processing', 'Preparing', 'Packing', 'For Delivery', 'In Transit', 'Delivered'];

                return (
                    <Card key={order._id} className="bg-dark my-3">
                        <Card.Body>
                            <Row>
                                <Col className='text-warning mt-2'>
                                    <ul className="list-unstyled">
                                        <li><strong>Ref ID: </strong><span className='text-light ellipsis'>{order.
referenceId}</span></li>
                                        <li><strong>Record State:</strong> <span className={order.isActive ? "text-info" : "text-danger"}>{order.isActive ? "Active" : "Inactive"}</span></li>
                                        <li><strong>Order ID: </strong><span className='text-light ellipsis'>{order._id}</span></li>
                                        <li><strong>Order Date: </strong><span className='text-light'>{order.orderDate}</span></li>
                                        <li><strong>Total Items: </strong><span className='text-light'>{order.totalItems}</span></li>
                                        <li><strong>Total Amount: </strong><span className='text-light'>{FormatCurrency(order.totalAmount)}</span></li>
                                        <li><strong>Actions: </strong><span className='text-light'>User {order.userActions}</span></li>
                        
                                    </ul>
                                </Col>
                         
                                <Col className='text-warning mt-2'>
                                    <ul className="list-unstyled">
                                        <li><strong>User ID: </strong><span className='text-light'>{order.userId}</span></li>
                                        <li><strong>Customer Name: </strong><span className='text-light'>{order.fullName}</span></li>
                                        <li><strong>Email: </strong><span className='text-light'>{order.email}</span></li>
                                        <li><strong>Mobile No: </strong><span className='text-light'>{order.mobileNo}</span></li>
                                        <li><strong>Delivery Address: </strong><span className='text-light'>{order.address}</span></li>
                                    </ul>
                                </Col>
                          
                                <Col className='text-warning mt-2'>
                                    <Accordion defaultActiveKey={['0']}>
                                        <Accordion.Item eventKey="1">
                                            <Accordion.Header>Orders</Accordion.Header>
                                            <Accordion.Body className='bg-dark'>
                                                <span className='text-light'>{order.products.map((product, index) => (
                                                    <div key={index}>
                                                        <Accordion>
                                                            <Accordion.Item eventKey={`item-${index}`}>
                                                                <Accordion.Header>{`Items ${product.name}`}</Accordion.Header>
                                                                <Accordion.Body className='bg-dark text-white'>
                                                                    <Row><Col>Product ID: {product.productId}</Col></Row>
                                                                    <Row><Col>Price: ${product.price}</Col></Row>
                                                                    <Row><Col>Quantity: {product.quantity}</Col></Row>
                                                                    <Row><Col>Subtotal: ${product.subtotal}</Col></Row>
                                                                    <Row><Col>Is Ordered: {product.isOrdered ? 'Yes' : 'No'}</Col></Row>
                                                                </Accordion.Body>
                                                            </Accordion.Item>
                                                        </Accordion>
                                                    </div>
                                                ))}</span>
                                            </Accordion.Body>
                                        </Accordion.Item>
                                    </Accordion>
                                </Col>
                               
                                <Col className='text-warning mt-2'>
                               
                                    <ul className="list-unstyled">
                                        <li>
                                            <strong>Order Status: </strong>
                                            <div className='text-center'>
                                                {statusOptions.map((option, index) => (
                                                    <p key={index} className={option === order.orderStatus ? 'selected-status py-1 text-center' : 'px-1 other-status text-start'}>
                                                        {option === order.orderStatus ? ` ${option}` : option}
                                                    </p>
                                                ))}
                                            </div>
                                        </li>
                                    </ul>
                                </Col>
                              
                                <Col className="mt-2 align-content-center">
                                    <UpdateOrderStatus order={order._id} fetchOrderData={fetchOrderData} />
                                </Col>
                                </Row>
                        </Card.Body>
                    </Card>
                );
            });

    setOrders(ordersArr);
    }, [order]);
    
    return (
    <>
    {orders}       
    </>
    );
}