import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Tab, Tabs, Button, Modal, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import FormatCurrency from '../FormatCurrency';
import Swal from 'sweetalert2';

export default function AllOrders({ fetchOrderData, order}) {
    // Initialize the orders state

const [ordersAll, setOrders] = useState([]);

let token = localStorage.getItem('token');

  fetch(`${process.env.REACT_APP_API_URL}/orders/all`,{
    headers: {
      "Content-Type": "application/json",
      'Authorization': `Bearer ${token}`
    },
  })
    .then((res) => res.json())
    .then((data) => {
      setOrders(order);
    });


    return (
        <>
        <Row className="justify-content-center">
            
        {ordersAll.map((order) => (
          <Col key={order._id} xs={12} sm={6} md={4} lg={3} className="vh-auto my-4">
            {(!order.isActive ? 
                <>
                  <Link className="btn btn-secondary text-white" to={`./#`}>
                  {order.name}
                  <img className="img-fluid border rounded mb-2" src={order.productImg} alt={order.name} />
                  <i className="bowl-rice fa-solid fa-bowl-rice"></i> - {FormatCurrency(order.price)}
                  </Link>
                  </>
                  : 
                  <>
                  <Link className="btn btn-dark text-white" to={`./${order._id}`}>
                  {order.name}
                  <img className="img-fluid border rounded mb-2" src={order.productImg} alt={order.name} />
                  <i className="bowl-rice fa-solid fa-bowl-rice"></i> - {FormatCurrency(order.price)}
                  </Link>
                  
                  </>
                  )}
          </Col>
     
        ))}
        </Row>
    </>
    )
}
