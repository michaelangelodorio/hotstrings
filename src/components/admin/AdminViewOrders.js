import React, { useState } from 'react';
import OrderTable from './OrderTable';
import { Container, Row, Col } from 'react-bootstrap';

export default function AdminViewOrders({ fetchOrderData, order}) {

    const [orders, setOrders] = useState([]);

    return (
        <>
        <Container className='pt-5 my-5' id="admin-dashboard">
        <Row>
            <Col>
                <Container className="text-center mb-3">
                    <Row>
                        <Col>
                        <h1 className="d-none d-sm-block text-light">Dashboard Orders</h1>
                        <h3 className="d-block d-sm-none text-light">Dashboard Orders</h3>
                        </Col>
                    </Row>
                </Container>
            </Col>
        </Row>      

        <Row>
            <Col>
                <Container className=''>
                    <OrderTable fetchOrderData={fetchOrderData} order={order}/>
                </Container>
            </Col>
        </Row>
    </Container>

</>
     
    );
}