import { Button, Container, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function UpdateRole({users, isAdmin, fetchUserData}) {

    const userToggle = (users) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/${users}/userrole`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })

        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data === true) {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'User successfully disabled'
                })
                fetchUserData();

            }else {
                Swal.fire({
                    title: 'Something Went Wrong',
                    icon: 'Error',
                    text: 'Please Try again'
                })
                fetchUserData();
            }

        })
        
    console.log(users)
    console.log(isAdmin)
    }
        const adminToggle = (users) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/${users}/adminrole`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })

        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data === true) {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'User successfully enabled'
                })
                fetchUserData();
            }else {
                Swal.fire({
                    title: 'Something Went Wrong',
                    icon: 'Error',
                    text: 'Please Try again'
                })
                fetchUserData();
            }


        })
        
    console.log(users)
    console.log(isAdmin)
    }
 

    
    return(
        <>
  
			
            {isAdmin ?

                <Button variant="secondary" size="sm" onClick={() => userToggle(users)}>Set as User</Button>

                :

                <Button variant="warning" size="sm" onClick={() => adminToggle(users)}>Set as Admin</Button>

            }
        
        
        </>

        )
}