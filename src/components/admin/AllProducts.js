import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Tab, Tabs, Button, Modal, Form } from 'react-bootstrap';
import FormatCurrency from '../global/FormatCurrency';
import Swal from 'sweetalert2';

export default function AllProducts({ user, fetchProductData, products }) {
  const [error, setError] = useState(null); // New state for error handling
  const [productAll, setProducts] = useState([]);

  useEffect(() => {
    fetchProductData();
    setProducts(products)
}, [products]);

// console.log("All",productAll);
  // console.log("All Products ",products);

  return (
    <>
      {error ? (
        <p>Error: {error}</p>
      ) : (
        <Row className="justify-content-center">
          {products.map((product) => (
            <Col key={product._id} xs={10} sm={10} md={5} lg={4} xl={2} className="m-3">
              {!product.isActive ? (
                <>
                  <Link className="btn btn-secondary text-white" to={`./#`}>
                  <div>{product.name}</div>
                  <div><img className="imgthumbnail-lg img-fluid my-2" src={product.productImg} alt={product.name} /></div>
                  <div><i className="bowl-rice fa-solid fa-bowl-rice"></i> - {FormatCurrency(product.price)}</div>
                  </Link>
                </>
              ) : (
                <>
                <div className="">
                <Link className="btn btn-dark text-white" to={`/products/${product._id}`}>
                  <div>{product.name}</div>
                  <div><img className="imgthumbnail-lg img-fluid my-2" src={product.productImg} alt={product.name} /></div>
                  <div><i className="bowl-rice fa-solid fa-bowl-rice"></i> - {FormatCurrency(product.price)}</div>
                </Link>
                </div>
                </>
              )}
            </Col>
          ))}
        </Row>
      )}
    </>
  );
}
