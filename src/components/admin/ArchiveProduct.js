
import { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveProduct({fetchProductData, products, isActive}){

	const archiveToggle = (e, products)=>{

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/${products}/archive`,{

			method:'PUT',
			headers:{
				'Content-Type':'application/json',
				'Authorization':`Bearer ${localStorage.getItem('token')}`
			}
			
		})
		.then(res=>res.json())
		.then(data=>{

			// console.log("this is a: ", product)

			if(data === true){
				Swal.fire({
					title:'Success!',
					icon:'info',
					text:'Archived Successfully.'
				})
				
                fetchProductData();
			}else{
				Swal.fire({
					title:'Error!',
					icon:'error',
					text:'Please try again'
				})
				
                fetchProductData();
			}
		})

	}

	const activateToggle = (e, products)=>{

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/${products}/activate`,{

			method:'PUT',
			headers:{
				'Content-Type':'application/json',
				'Authorization':`Bearer ${localStorage.getItem('token')}`
			}
			
		})
		.then(res=>res.json())
		.then(data=>{


			if(data === true){
				Swal.fire({
					title:'Success!',
					icon:'success',
					text:'Product Successfully Activated'
				})
				
                fetchProductData();
			}else{
				Swal.fire({
					title:'Error!',
					icon:'error',
					text:'Please try again'
				})
				
                fetchProductData();
			}
		})

		
	}

	return (
		<>

		{isActive ? 
			<Button variant="success" size="sm" onClick={e=>archiveToggle(e,products)}>Active</Button>
			:
			<Button variant="secondary" size="sm" onClick={e=>activateToggle(e,products)}>Inactive</Button>	            
		}

		</>


	)
}