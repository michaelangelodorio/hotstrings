import { Button, Container, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function UserActivation({users, isActivated, fetchUserData}) {
  
	const deactivateToggle = (e, users)=>{

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/${users}/activate`,{

			method:'PUT',
			headers:{
				'Content-Type':'application/json',
				'Authorization':`Bearer ${localStorage.getItem('token')}`
			}
			
		})
		.then(res=>res.json())
		.then(data=>{

			console.log("this is a: ", users)

			if(data === true){
				Swal.fire({
					title:'Deactivation:',
					icon:'info',
					text:'User account is now Deactivated.'
				})
				
                fetchUserData();
			}else{
				Swal.fire({
					title:'Error!',
					icon:'error',
					text:'Please try again'
				})
				
                fetchUserData();
			}
		})

	}

	const activateToggle = (e, users)=>{

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/${users}/deactivate`,{

			method:'PUT',
			headers:{
				'Content-Type':'application/json',
				'Authorization':`Bearer ${localStorage.getItem('token')}`
			}
			
		})
		.then(res=>res.json())
		.then(data=>{


			if(data === true){
				Swal.fire({
					title:'Activation:',
					icon:'success',
					text:'User Successfully Activated'
				})
				
                fetchUserData();
			}else{
				Swal.fire({
					title:'Error!',
					icon:'error',
					text:'Please try again'
				})
				
                fetchUserData();
			}
		})

		
	}

	return (
		<>

		
		{isActivated ? 
			<Button block="true" variant="danger" size="sm" onClick={e=>deactivateToggle(e,users)}>Deactivate User</Button>
			:
			<Button block="true" variant="success" size="sm" onClick={e=>activateToggle(e,users)}>Activate User</Button>	            
		}
	


		</>

        )
}