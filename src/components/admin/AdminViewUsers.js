import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Table } from 'react-bootstrap';
import UserView from '../user/UserView';

export default function AdminViewUsers({ users, fetchUserData }) {

   
    return (
    <>
    <Container className='pt-5 my-5' id="admin-dashboard">
        <Row>
            <Col md={12} className='align-items-center align-content-center'>
                <h1 className="text-white text-center">Dashboard - Users</h1>
            </Col>
        </Row>
        <Row>
            <Col>
                <Container className='d-block' bordered="true" responsive="true" size="sm" variant="dark">
                    <Row>
                    <Col md={12} >
                    <h2 className='text-warning text-center'>Users ({users.length})</h2>
                        <UserView fetchUserData={fetchUserData} users={users} />
                    </Col>
                    </Row>
                </Container>
            </Col>
        </Row>
    </Container>              
    </>
     
    );
}