import { useState, useEffect } from 'react';
import { Col, Container, Row, Table } from 'react-bootstrap';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct'
import AddProduct from './AddProduct';
import ProductTable from './ProductTable';
import ProductTableMobile from './ProductTableMobile';

export default function AdminViewProducts({ fetchProductData, products, endpoint }) {

    return (
    <>
    <Container className='pt-5 my-5' id="admin-dashboard">
    <Row>
            <Container className="text-center mb-3">
        <Row>
            <Col md={{ span: 4, offset: 4 }}>
            <h1 className="d-none d-sm-block text-light">Dashboard Products</h1>
            <h3 className="d-block d-sm-none text-light">Dashboard Products</h3>
            </Col>
        </Row>
        <Row>
            <Col md={{ span: 4, offset: 4 }}>
            <div className="d-grid text-warning">
                Add New Product
                <AddProduct fetchProductData={fetchProductData} />
            </div>
            </Col>
        </Row>
        </Container>

            <Col md={12} >
            <Table className='d-none d-sm-block' bordered="true" responsive="true" size="sm" variant="dark">
                <thead>
                
                    <tr className="text-center">
                        <th><span className='text-warning text-start'>Products: ({products.length})</span></th>
                    
                        <th>Data:</th>
                        <th colSpan="2">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <ProductTable fetchProductData={fetchProductData} products={products}/>
                </tbody>
            </Table>
            </Col>

            <Col md={12} >
                
                    <Container className='d-block d-sm-none' bordered="true" responsive="true" size="sm" variant="dark">
                        <Row>
                            <Col>
                            <span className='text-warning text-start'>Products ({products.length})</span>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                            <Container>
                            <ProductTableMobile fetchProductData={fetchProductData} products={products}/>
                            </Container>
                            </Col>
                        </Row>
                    </Container>
            </Col>
        </Row>
    </Container>

    </>
     
    );
}