import React, { useState } from 'react';
import { Button, Modal, Form, Container, Col, Row } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Navigate, useNavigate } from 'react-router-dom';


export default function AddProduct({products , fetchProductData}){

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [productImg, setProductImg] = useState('');
	const [quantity, setQuantity] = useState(0);
	const [showModal, setShowModal] = useState(false);
    const [isActive, setIsActive] = useState(false);
	const [ratings, setRating] = useState([]);
	const [averageRating, setAverageRating] = useState(0);
    const navigate = useNavigate();

	const openModal = (x) =>{
	
			setName("");
			setDescription("");
			setPrice(0);
			setProductImg("");
			setQuantity(0);
			setRating([]);
            setIsActive(false);
			setShowModal(true);
			setAverageRating(0);

	}

	const closeModal = () =>{

		setShowModal(false);
		setName('');
		setDescription('');
		setProductImg('');
		setPrice(0);
		setQuantity(0);
        setIsActive(false);
		setRating([]);
        fetchProductData();
		setAverageRating(0);
	}

	const createProduct = (e)=>{

		e.preventDefault();

        let token = localStorage.getItem('token');
		fetch(`${process.env.REACT_APP_API_URL}/products/add`,{

			method:'POST',
			headers:{
				'Content-Type':'application/json',
				'Authorization':`Bearer ${token}`
			},
			body:JSON.stringify({
				name: name,
                description: description,
                price: price,
                productImg : productImg,
                quantity : quantity,
				ratings : "",
                isActive : false,
				averageRating: 0
			})
		})
		.then(res=>res.json())
		.then(data=>{
	
			if(data){
                Swal.fire({

                    icon:"success",
                    title: "Product Added"

                })
                closeModal();
                fetchProductData();
                navigate("/products");
            } else {
                Swal.fire({

                    icon: "error",
                    title: "Unsuccessful Product Creation",
                    text: data.message

                })
                fetchProductData();
            }
		})

	}

	return(
		<>
		<Container>
			<Row>
				<Col>
				<Button variant="warning" block="false" size="lg" onClick={()=>openModal()}>
					<i className="fa-solid fa-circle-plus"></i>
				</Button>
				</Col>
			</Row>
		</Container>
			<Modal show={showModal} onHide={closeModal}>
				<Form onSubmit={e=>createProduct(e)}>

					<Modal.Header closeButton>
						<Modal.Title>Add New Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>

						<Form.Group controlId="productName">
							<Form.Label>Product Name</Form.Label>
							<Form.Control
								type="text"
								value={name}
								onChange={e=>setName(e.target.value)}
								required/>
						</Form.Group>

						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								type="text"
								value={description}
								onChange={e=>setDescription(e.target.value)}
								required/>
						</Form.Group>

						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								type="number"
								value={price}
								onChange={e=>setPrice(e.target.value)}
								required/>
						</Form.Group>

						<Form.Group controlId="setProductImg">
							<Form.Label>Image Url</Form.Label>
							<Form.Control
								type="text"
								value={productImg}
								onChange={e=>setProductImg(e.target.value)}
								/>
						</Form.Group>

						<Form.Group controlId="productQuantity">
							<Form.Label>Quantity</Form.Label>
							<Form.Control
								type="number"
								value={quantity}
								onChange={e=>setQuantity(e.target.value)}
								required/>
						</Form.Group>

					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeModal}>Close</Button>
						<Button variant="warning" type="submit">Submit</Button>
					</Modal.Footer>

				</Form>
			</Modal>

		</>

	)


}
