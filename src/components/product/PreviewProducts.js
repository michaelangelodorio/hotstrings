import React, { useEffect, useState, useContext } from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function PreviewProducts({ breakPoint, productData }) {
  const { _id, name, description, price, productImg } = productData;

  return (
    <>
    <Col
        xs={{ span: 12, offset: 0 }}
        md={{ span: 6, offset: 0 }}
        lg={{ span: 6, offset: 0 }}
        xl={{ span: 3, offset: 0 }} className='pb-5 blur-content-half-bg-dark align-items-center'
      >

      <Container className="product-card pb-4 mx-2 bg-dark text-warning">
      <Row>
        <Col>
          <Link to={`/products/${_id}`}>
            <Card.Img className='imgthumbnail-md img-fluid' variant="top" src={productImg} />
          </Link>
        </Col>
      </Row>

      <Row>
        <Col>
          <Card.Title className="ellipsis text-center">
            <Link className='text-white' to={`/products/${_id}`}>{name}</Link>
          </Card.Title>
          <Card.Text className="desc ellipsis">{description}</Card.Text>
        </Col>
      </Row>

      <Row>
        <Col>
        <Link className="my-2 btn btn-primary d-block" to={`/products/${_id}`}>
          Details
        </Link>
        </Col>
      </Row>
    
      </Container>
    </Col>

  </>

  );
}