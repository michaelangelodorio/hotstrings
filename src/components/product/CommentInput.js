import React, { useState, useEffect, useContext } from "react";
import UserContext from "../../UserContext";
import { Col, Container } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function CommentInput({ productId, onRatingChange, fetchProductData }) {
  const [comment, setComment] = useState('');
  const [rate, setRate] = useState(0);
  const { user } = useContext(UserContext);
  const [message, setMessage] = useState('');
  const [isVisible, setIsVisible] = useState(true);

  
  const handleCommentChange = (e) => {
    setComment(e.target.value);
  };

  const handleClick = (starCount) => {
    setRate(starCount);
    onRatingChange(starCount);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (user.userImg === "" || user.userImg === null  || user.userImg === undefined)
    {
        user.userImg = "https://cdn.dribbble.com/users/9685/screenshots/997495/avatarzzz.gif"
      }

    const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/comments`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        userId: user._id,  // Replace with actual userId
        userImg: user.userImg, // Replace with actual userImg
        userName: `${user.firstName} ${user.lastName}`, // Replace with actual userName
        comment,
        rate: rate, // Convert to integer
        date: Date.now(), // Use actual date logic
        isActive: 'active' // Replace with actual isActive value
      }),
    },[]);

    if (response.ok) {

      if (response) {

        setIsVisible(true);
        setMessage('Review/Rating added Successfully!');
        setComment('');
        setRate(0);

      } else {

        setIsVisible(true);
        setMessage('Oh No! Unable to rate. Please try again.');
        setComment('');
        setRate(0);
      }

    }
  };



useEffect(() => {
  // Set a timeout to hide the popup after 3 seconds
  const timeout = setTimeout(() => {
    setIsVisible(false);
  }, 3000);

  fetchProductData();
  return () => clearTimeout(timeout);

}, [rate, comment]);

  return (
    <div className="comment-input">
      <form onSubmit={handleSubmit}>
        <div className="form-group  my-3 ">
          <h5>Comment:</h5>
          <textarea
            id="comment"
            className="form-control"
            value={comment}
            onChange={handleCommentChange}
            required
          ></textarea>
        </div>

      <Col xs={12} lg={6}>  
      <div className="my-3">
      <h5>Rate:
        {[1, 2, 3, 4, 5].map((star) => (
          <span
            key={star}
            className={star <= rate ? 'text-warning mx-2' : 'text-secondary mx-2'}
            style={{ cursor: 'pointer' }}
            onClick={() => handleClick(star)}
          >&#9733;</span>
        ))}</h5>
      </div>
 
        <button type="submit" className="my-2 btn btn-primary">
          Submit My Rate
        </button>
        <div className={`popup ${isVisible ? 'visible' : 'hidden'}`}>
          {message && <div className="alert alert-warning">{message}</div>}
        </div>
      </Col>
      </form>
    </div>
  );
};
