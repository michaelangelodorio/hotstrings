import React, { useState, useEffect } from 'react';
import { Col, Row, Container, Button, Modal, Form } from 'react-bootstrap';
import FormatCurrency from '../global/FormatCurrency';
import { Link } from 'react-router-dom';

const ProductSearch = () => {
  // State variables
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [products, setProducts] = useState([]);

  const searchByName = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/searchByName`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ productName: searchQuery })
      });
      const data = await response.json();
      setProducts(data);

    } catch (error) {
      console.error('Error searching for products by name:', error);
    }
  };
  
  useEffect(() => {
    searchByName();
}, [searchQuery]);

  return (
    <Container fluid="true" className='vh-auto mb-4'>
    <Row className="justify-content-center text-white">
    <Col xs={10} sm={10} md={5} lg={4} xl={2} className="m-3">
        <Form.Group>
          <h3><i className="bowl-rice fa-solid fa-bowl-rice"></i> Search</h3>
          <input
            type="text"
            id="productName"
            className="form-control mb-5"
            value={searchQuery}
            placeholder='Search by name'
            onChange={(event) => setSearchQuery(event.target.value)}
          />
        </Form.Group>
      </Col>
    </Row>
    <Row className="justify-content-center">
      {products.map((product) => (
      <Col key={product._id} xs={10} sm={10} md={5} lg={4} xl={2} className="m-3">
        <div className="">
        <Link className="btn btn-dark text-white" to={`/products/${product._id}`}>
          <div>{product.name}</div>
          <div><img className="imgthumbnail-lg img-fluid py-2" src={product.productImg} alt={product.name} /></div>
          <div><i className="bowl-rice fa-solid fa-bowl-rice"></i> - {FormatCurrency(product.price)}</div>
        </Link>
        </div>
      </Col>
      ))}
    </Row>
  </Container>
  );
};

export default ProductSearch;
