import { useState, useEffect } from 'react';
import { CardGroup, Container } from 'react-bootstrap';
import PreviewProducts from './PreviewProducts';

export default function FeaturedProducts({breakPoint}){
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState(null);
	const [previews, setPreviews] = useState([]);

  useEffect(() => {
    // Simulate AI processing
    setTimeout(() => {
      // Fetch data or perform AI-related tasks here
      const fetchData = async () => {
        try {
          const response = await fetch(`${process.env.REACT_APP_API_URL}/products`);
          const data = await response.json();
  
          const generateRandomNums = () => {
            let randomNum;
            do {
              randomNum = Math.floor(Math.random() * data.length);
            } while (numbers.indexOf(randomNum) !== -1);
  
            numbers.push(randomNum);
          };
  
          const numbers = [];
          const featured = [];
          let productLength = Math.min(4, data.length);
  
          for (let i = 0; i < productLength; i++) {
            generateRandomNums();
            featured.push(
              <PreviewProducts productData={data[numbers[i]]} key={data[numbers[i]]._id} breakPoint={breakPoint} />
            );
          }
  
          setPreviews(featured);
  
          // Call generateRandomNums every 5 seconds
          const intervalId = setInterval(() => {
            numbers.length = 0; // Reset numbers array
            for (let i = 0; i < productLength; i++) {
              generateRandomNums();
              featured[i] = (
          <PreviewProducts
          productData={data[numbers[i]]}
          key={data[numbers[i]]._id}
          className="blur-content-half-bg-dark" // Add this line
          />
              );
            }
        
            setPreviews([...featured]); // Update previews with new random products
          }, 8000);
  
          return () => {
            clearInterval(intervalId); // Cleanup the interval when component unmounts
          };
        } catch (error) {
          console.error('Error fetching data:', error);
        }
      };
  
      fetchData();
      setIsLoading(false);
    }, 2000); // Simulate 2 seconds of processing time
  }, []);


	return(
    <div>
    {isLoading ? (
                <div className="bg-dark centered-container">
                <img width={100}
                  src="https://usagif.com/wp-content/uploads/loading-12.gif"
                  alt="Centered Image"
                  className="centered-image"
                />
              </div>
    ) : (
      <div>
      <CardGroup className="justify-content-center align-items-center">
        {previews}
      </CardGroup>
      </div>
    )}
  </div>

	)
}