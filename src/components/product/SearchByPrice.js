import React, { useState, useEffect } from 'react';
import { Col, Row, Container, Button, Modal, Form } from 'react-bootstrap';
import FormatCurrency from '../global/FormatCurrency';
import { Link } from 'react-router-dom';

const SearchByPrice = () => {
  
  const [minPrice, setMinPrice] = useState(0);
  const [maxPrice, setMaxPrice] = useState(300);
  const [products, setProducts] = useState([]);

  const handleChange = (event) => {
    setMinPrice(1);
    setMaxPrice(event.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ minPrice, maxPrice }),
    };

    fetch(`${process.env.REACT_APP_API_URL}/products/searchByPrice`, requestOptions)
      .then((response) => response.json())
      .then((data) => {
        setProducts(data.products);
 
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  };

  return (
    <Container fluid="true" className='vh-auto mb-4'>
      <Row className="justify-content-center text-white">
        <Col  xs={10} sm={10} md={5} lg={4} xl={3} className="m-3">
          <h2>
            Search <i className="bowl-rice fa-solid fa-bowl-rice"></i> by Price Range
          </h2>
          <form onSubmit={handleSubmit}>
            <input
              type="range"
              id="price"
              className="form-range my-3"
              min={1}
              max={999}
              value={maxPrice}
              onChange={handleChange}
            />
            <div className="d-grid gap-2 py-3">
              <Button type="submit" variant="primary" size="lg">
                {FormatCurrency(maxPrice)} Search
              </Button>
            </div>
          </form>
        </Col>
      </Row>
      <Row className="justify-content-center">
        {products.map((product) => (
          <Col key={product._id} xs={10} sm={10} md={5} lg={4} xl={2} className="m-3">
            <div className="">
            <Link className="btn btn-dark text-white" to={`/products/${product._id}`}>
              <div>{product.name}</div>
              <div><img className="imgthumbnail-lg img-fluid my-2" src={product.productImg} alt={product.name} /></div>
              <div><i className="bowl-rice fa-solid fa-bowl-rice"></i> - {FormatCurrency(product.price)}</div>
            </Link>
            </div>
          </Col>
        ))}
      </Row>
    </Container>
  );
};

export default SearchByPrice;