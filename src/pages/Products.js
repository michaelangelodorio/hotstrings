import React, { useEffect, useState, useContext } from 'react';
import { Container, Row, Col, CardGroup } from 'react-bootstrap';

import UserContext from '../UserContext';
import UserProduct from '../components/user/UserProduct';
import AdminViewProducts from '../components/admin/AdminViewProducts';

export default function Products() {
  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([]);
  const [allProducts, setAllProducts] = useState([]);
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState(null);

  const fetchProductData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/`)
      .then((res) => {
        if (!res.ok) {
          throw new Error('Network response was not ok');
        }
        return res.json();
      })
      .then((data) => {
        setProducts(data);
      })
      .catch((error) => {
        setError('Error fetching product data. Please try again later.');
      });
  }

  const fetchAllProductData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((res) => {
        if (!res.ok) {
          throw new Error('Network response was not ok');
        }
        return res.json();
      })
      .then((data) => {
        setAllProducts(data);
      })
      .catch((error) => {
        setError('Error fetching all product data. Please try again later.');
      });
  }

  useEffect(() => {
    // Simulate AI processing
    setTimeout(() => {
      // Fetch data or perform AI-related tasks here
      fetchProductData();
      fetchAllProductData();
      setIsLoading(false);
    }, 30); // Simulate 2 seconds of processing time
  }, []);


  const renderView = () => {
    if (error) {
      return <div>{error}</div>;
    }

    if (user.isAdmin) {
      return (
        <Container fluid="true" >
          <Row>
            <Col>
              <AdminViewProducts products={allProducts} fetchProductData={fetchAllProductData} />;
            </Col>
          </Row>
        </Container>
      )
    } else {
      return (
        <Container fluid="true" className='bg-custom'>
          <Row>
            <Col>
              <UserProduct products={products} user={user} fetchProductData={fetchProductData}/>
            </Col>
          </Row>
        </Container>
      );
    }
  }

  return (
    <div>
      {isLoading ? (
          <div className="bg-dark centered-container">
          <img width={100}
            src="https://usagif.com/wp-content/uploads/loading-12.gif"
            alt="Centered Image"
            className="centered-image"
          />
        </div>
      ) : (
        <div>
        {renderView()}
        </div>
      )}
      </div>
    )
}