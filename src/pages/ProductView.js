import React, { useState, useEffect, useContext } from "react";
import { Container, Card, Button, Row, Col, ButtonGroup} from "react-bootstrap";
import { useParams, Link } from "react-router-dom";
import Swal from 'sweetalert2';
import UserContext from "../UserContext";
import formatCurrency from '../components/global/FormatCurrency';
import formatDate from '../components/global/FormatDate';
import CommentInput from '../components/product/CommentInput';
import FeaturedProducts from "../components/product/FeaturedProducts";
import CardHeader from "react-bootstrap/esm/CardHeader";

export default function ProductView() {
  // Context and Params
  const { user } = useContext(UserContext);
  // const [user, setUser] = useState([]);
  let token = localStorage.getItem('token');
  const { productId } = useParams();

  // console.log(user)
  // State

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [img, setImage] = useState("");
  const [itemsToBuy, setItemsToBuy]= useState(0);
  //User input for quantity
  const [quantity, setQuantity] = useState(0);
  const [ratings, setRatings] = useState([]);
  const [averageRating, setAverageRating] = useState([]);
  const subtotal = formatCurrency(price * itemsToBuy);

  const handleRatingChange = (starCount) => {
    console.log(`User rated with ${starCount} stars`);
  };

const fetchProductData = () => {
  fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
  .then(res => res.json())
  .then(data => {
    setName(data.name);
    setDescription(data.description);
    setPrice(data.price);
    setImage(data.productImg);
    setItemsToBuy(0);
    setQuantity(data.quantity);
    setRatings(data.ratings);
    setAverageRating(data.averageRating);
  })
  .catch(error => {
    console.error('Error fetching data:', error);
  });
}

  const addToCart = () => {

    fetch(`${process.env.REACT_APP_API_URL}/carts/add`, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        productId: productId,
        quantity: itemsToBuy
        
      })
    })
      .then((res) => res.json())
      .then((data) => {
  
        if (data) {
          Swal.fire({
            title: "Successfully added.",
            icon: 'success',
            text: "You have successfully added the product."
          });
          
          updateProductQuantity();

        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: 'error',
            text: "Please try again."
          })
        }
        
        console.log(data);
  
      })
  }
  
  const updateProductQuantity = ()=>{

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`,{

			method:'PUT',
			headers:{
				'Content-Type':'application/json',
				'Authorization':`Bearer ${localStorage.getItem('token')}`
			},
			body:JSON.stringify({
        quantity : quantity,
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)
      reset();
		})
	}


function add() {
  if (quantity > 0) {
    setItemsToBuy(prevItems => prevItems + 1);
    setQuantity(prevInventory => prevInventory - 1);
  }
}

  function remove() {
    if (itemsToBuy > 0) {
      setItemsToBuy(prevItems => prevItems - 1);
      setQuantity(prevInventory => prevInventory + 1);
    }
  }

  function reset() {
    fetchProductData();
  }

  useEffect(() => {
    fetchProductData();
    // fetchUser();
  }, [productId]);


  return (
  <div className="product-view">
    <Container>
      <Row>
        <Col className="mt-5"></Col>
      </Row>
      <Row>
        <Col xs={12} lg={6} className="mt-5">
          <Card className='bg-dark text-white '>
            <CardHeader><h3 className="text-white pt-3">{name}</h3></CardHeader>
            <Card.Body className="text-white">
              <Card.Img variant="top" className='my-3 object-fit-cover border rounded' src={`${img}`} />
              {user.id !== null && (
             
              <div className='p-2 d-flex align-items-center justify-content-between'>
                <Card.Title><span className="text-warning h3"><strong>Available:</strong></span> <span className="text-white h3">{quantity}</span></Card.Title>
                <Card.Text className='h4 text-white'>Price: {formatCurrency(price)}</Card.Text>
                <ButtonGroup aria-label="Basic example">
                <div className="d-flex">
                <Link title="decrease quantity" className='m-1 px-3' variant="danger" onClick={remove}>
                <i className="fa-regular fa-square-minus"></i>
                </Link>
           
                <Card.Text className='m-1 h4'>{itemsToBuy}</Card.Text>

                <Link title="decrease quantity" className='m-1 px-3' variant="warning" onClick={add}>
                <i className="fa-solid fa-square-plus"></i>
                </Link>
                </div>
                </ButtonGroup>
                </div>
              )}
              <div className='p-2'>
              <Card.Text><span className="h5 text-warning">Description: </span>
             {description}</Card.Text>

              {user.id !== null ? (
              <>
              <Card.Text className='text-warning h4'><strong>TOTAL:</strong><span className="text-light">&nbsp;{subtotal}</span></Card.Text>
              
              <div className='p-2 d-flex align-items-center justify-content-between'>
              {itemsToBuy > 0 ? (
                <Button title="Add to cart!" variant="success" onClick={() => addToCart(productId, quantity)}>
                Add to Cart
                </Button>
              ) : (
                <Button title="Add to cart!" variant="success" disabled onClick={() => addToCart(productId, quantity)}>
                Add to Cart
                </Button>
              )}

              <Button title="Reset selections" className="btn btn-danger d-block" onClick={() => reset()}>
              Reset
              </Button>
              </div>
              </>
              ) : (
              <>
              <Link className="btn btn-danger d-block" to="/users/login">
              Log in to order
              </Link>    
              </>
              )}
              </div>
              <div className="mt-5">
                <Card.Text>
                  <span className="h4 text-warning">Must try:</span>
                </Card.Text>
                <FeaturedProducts breakpoint={3} />
              </div>
            </Card.Body>
          </Card>
        </Col>
          <Col xs={12} lg={6} className="mt-5">
          <Card className='bg-dark text-white'>
            <Card.Body>
              <Card.Text>
            
              </Card.Text>
              {user.id ? (
              <>
                <h3 className="text-warning">Review/Ratings:&nbsp;
                 {Array.from({ length: averageRating }, (_, index) => (
                        <span key={index}>&#9733;</span>
                ))}
                </h3>
                <CommentInput 
                  onRatingChange={handleRatingChange} 
                  fetchProductData={fetchProductData} 
                  productId={productId} 
                />
              </>
            ) : (
              <div className="d-flex justify-content-center align-items-center">
                <div>
                  <Link className="btn btn-warning" to="/users/login">
                    Log in to Rate
                  </Link>
                </div>
              </div>
            )}
              <div className="m-1 comment-section">
              {ratings.sort((a, b) => b.date - a.date).map((rating, index) => (
    
              <div key={index}>
                <div className="my-3">

                <Container>
                  <Row>
                  <Col md={12}>
                  <div className="me-5 text-end">
                    <span className="text-warning">Date:&nbsp;</span>
                    <span className="text-secondary">{formatDate(rating.date)}</span>
                  </div>
                  </Col>
                  </Row>

                  <Row>
                  <Col sm={2} md={2} lg={3} xl={2}>
                  <div className="text-center">
                   {rating.userImg === "" || rating.userImg === null  || rating.userImg === undefined ?(
                  rating.userImg = "https://cdn.dribbble.com/users/9685/screenshots/997495/avatarzzz.gif"
                  ) : (
                  <li>
                  <img title={`${rating.userName
}`} id="" className='imgthumbnail-sm m-2' src={rating.userImg} />
                  </li>
                  )}
                  </div>
                   </Col>
                  
                   <Col sm={8} md={10} lg={9} xl={10}>
                    <div className="comment-inner-section p-3">
                    {user.id === null ? (<div className="tex-center">
                 
                    {/* <span className="text-warning">User: </span> */}
                    <span className="text-white">Anon User</span>
                    </div>
                    ) : (
                    <>
                    {/* <span className="text-warning">User: </span> */}
                    <span className="text-white">{`${rating.userName}`}</span>
                    </>
                    )}
                    <li>
                      <span className="text-warning">Rate: </span>
                      {Array.from({ length: rating.rate }, (_, index) => (
                        <span key={index}>&#9733;</span>
                      ))}
                      </li>
                      <li>
                      <span className="text-warning">Comment: </span>
                      <span className="text-white">{`"${rating.comment}"`}</span>
                      </li>
                      
                    </div>
                    </Col>
            
                  </Row>
                  </Container>
                  
                  </div>
                </div>
                ))}
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  </div>
);
}
