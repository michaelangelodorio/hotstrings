import React, { useEffect, useState, useContext } from 'react';
import { Container, Row, Col, CardGroup } from 'react-bootstrap';
import UserContext from '../UserContext';
import UserCart from '../components/user/UserCart';
import { Placeholder, Alert } from 'react-bootstrap'; // Added Alert component

export default function Carts() {
  const { user } = useContext(UserContext);
  const [cart, setCart] = useState([]);
  const [error, setError] = useState(null); // Added state for error handling
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState(null);

  const fetchCartData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/carts`,{
        headers:{
            'Content-Type':'application/json',
            'Authorization':`Bearer ${localStorage.getItem('token')}`
        }
    })
      .then((res) => {
        if (!res.ok) {
          throw new Error('Network response was not ok');
        }
        return res.json();
      })
      .then((data) => {
        setCart(data);
      })
      .catch((error) => {
        setError(error.message);
      });
  }

  useEffect(() => {
    // Simulate AI processing
    setTimeout(() => {
      // Fetch data or perform AI-related tasks here
      fetchCartData();
      setIsLoading(false);
    }, 30); // Simulate 2 seconds of processing time
  }, [cart]);

  return (
    <div>
    {isLoading ? (
        <div className="bg-dark centered-container">
       <img width={100}
         src="https://usagif.com/wp-content/uploads/loading-12.gif"
         alt="Centered Image"
         className="centered-image"
       />
     </div>
    ) : (
      <>
       {error ? (
        <Alert variant="danger">
          {error}
        </Alert>
      ) : (
        <>
          {user.isAdmin ? (
            <></>
          ) : (
            <Container fluid="true" className='bg-custom'>
              <Row>
                <Col>
                  <UserCart fetchCartData={fetchCartData} cart={cart} />
                </Col>
              </Row>
            </Container>
          )}
        </>
      )}
      </>
    )}
  </div>
  );
}
