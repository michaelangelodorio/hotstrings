import React, { useState, useEffect, useContext } from 'react';
import Login from './Login';
import Banner from '../components/global/Banner';
import FeaturedProducts from '../components/product/FeaturedProducts';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';

export default function Home() {
    // Allow us to continue the User context object and it's properties to
    const {user, setUser} = useContext(UserContext);

  return (
    <>
      {user.id !== null ? (
        <Navigate to="/products" />
        ) : (
          <>
        <Login />
        <Banner />

    </>
   )}
 </>
);
}
