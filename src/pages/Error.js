import Banner from "../components/global/Banner"

export default function Error() {

    const data = {
      
        buttonText: "Back to home",
        linkTo: "/home"
    }

    return (
        <>
        <div className='d-flex justify-content-around align-items-center text-center text-warning vh-100'>
            <Banner data={data}/>
        </div>

        </>
	)
}
