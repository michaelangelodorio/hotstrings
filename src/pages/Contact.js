import React, { useState } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import { Navigate, useNavigate } from 'react-router-dom';


export default function Contact() {

  const [formData, setFormData] = useState({
    fullname: '',
    email: '',
    contactNumber: '',
    message: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleFormReset = () => {
    setFormData({
      fullname: '',
      email: '',
      contactNumber: '',
      message: '',
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
  
    // fetch('https://formsubmit.co/670d3901f0cce250278dea3ca5f66b47', {  // Replace with your FormSubmit endpoint
    //   method: 'POST',
    //   headers: {
    //     'Content-Type': 'application/json',
    //   },
    //   body: JSON.stringify({
    //     name: formData.fullname,  // Make sure field names match what FormSubmit expects
    //     email: formData.email,
    //     contact: formData.contactNumber,
    //     message: formData.message,
    //   }),
    // })
    //   .then((res) => res.json())
    //   .then((data) => {
    //     if (data.success) {
    //       setFormData({
    //         fullname: '',
    //         email: '',
    //         contactNumber: '',
    //         message: '',
    //       });
  
    //       Swal.fire({
    //         title: 'Email Sent!',
    //         icon: 'success',
    //         text: 'Thank you for contacting HotStrings!',
    //       });
    //       console.log(data);
    //     } else {
    //       Swal.fire({
    //         title: 'Email Delivery Failed.',
    //         icon: 'error',
    //         text: 'Check your input details and try again.',
    //       });
    //     }
    //   })
    //   .catch((error) => {
    //     console.error('Error:', error);
    //     // Handle error (e.g., display an error message)
    //   });
 
        Swal.fire({
          title: 'Email Sent!',
          icon: 'success',
          text: 'Thank you for contacting HotStrings!',
        });
  
        handleFormReset();
  };

  
  return (
    <>
      <Container fluid="true" className="bg-custom d-flex min-vh-100 py-5 justify-content-center align-items-center">
        <Row className="text-white">
          <Col
            xs={{ span: 12, offset: 0 }}
            md={{ span: 12, offset: 0 }}
            lg={{ span: 12, offset: 0 }}
            xl={{ span: 12, offset: 0 }}
            className="p-1 blur-content-half-bg-dark"
          >
            <h1 className="text-center text-white">Contact Us</h1>
            <div className="m-2 p-3 bg-color border border-secondary rounded-2">
              <form id="form-col" onSubmit={handleSubmit}>
                        <div className="form-group font-weight-bold">
                            <p className="h4 c-gold xlfont-min">You can Email me here:</p>
                            <label>Full Name:</label>
                            <input
                            className="form-control"
                            type="text"
                            name="fullname"
                            value={formData.fullname}
                            onChange={handleChange}
                            placeholder="Please enter your full name"
                            required
                            />
                        </div>

                        <div className="form-group font-weight-bold py-3">
                            <label>Email address:</label>
                            <input
                            className="form-control"
                            type="email"
                            name="email"
                            value={formData.email}
                            onChange={handleChange}
                            placeholder="Email address"
                            required
                            />
                        </div>

                        <div className="form-group font-weight-bold py-3">
                            <label>Contact Number: &nbsp;</label>
                            <span className="">&nbsp;[optional]</span>
                            <input
                            type="number"
                            className="form-control"
                            id="contactNumber"
                            name="contactNumber"
                            value={formData.contactNumber}
                            onChange={handleChange}
                            placeholder="Mobile/Tel number"
                            />
                        </div>

                        <div className="form-group tfont-weight-bold py-3">
                            <input
                            className="form-control"
                            type="hidden"
                            name="_subject"
                            value="You have a new email!"
                            />
                            <input
                            className="form-control"
                            type="hidden"
                            name="_template"
                            value="table"
                            />
                     
                            <label>Message:</label>
                            <textarea
                            className="form-control"
                            name="message"
                            id="message"
                            rows="3"
                            value={formData.message}
                            onChange={handleChange}
                            placeholder="Write your message here"
                            required
                            ></textarea>
                        </div>
                    
                <div className="pt-3">
                  <button className="btn btn-success mb-3 font-weight-bold" type="submit">
                    Send
                  </button>
                  <button
                    type="button"
                    className="btn btn-danger mb-3 font-weight-bold"
                    onClick={handleFormReset}
                  >
                    Reset Form
                  </button>
                </div>
              </form>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
}
