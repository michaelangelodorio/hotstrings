import {useState, useEffect, useContext } from 'react';
import {Row, Col, Container, Card } from 'react-bootstrap';
import ResetPassword from '../components/user/ResetPassword';
import UpdateProfile from '../components/user/UpdateProfile'
import ProfileTheme from '../components/user/ProfileTheme';
import UserContext
 from '../UserContext';
import CardHeader from 'react-bootstrap/esm/CardHeader';
export default function Profile(){
    const { user } = useContext(UserContext);
    const [ details, setDetails ] = useState({})
    const [ selectedColor, setSelectedColor ] = useState('');
    const [ divStyle, setDivStyle ] = useState({});
  
    const handleColorChange = (color) => {
      setSelectedColor(color);
      setDivStyle({
        backgroundColor: color,
      });

    fetch(`${process.env.REACT_APP_API_URL}/users/updateColor/${user}`, {
        method: 'PUT',
        headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify({ theme: color }),
    })
    .then(res => res.json())
    .then(data => {
        // Handle success or error response from the server if needed
        console.log('Color updated successfully:', data);
    })
    .catch(error => {
        console.error('Error updating color:', error);
    });
        // console.log(color)
    }
   

    useEffect(()=>{

        fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
            headers:{
                Authorization: `Bearer ${ localStorage.getItem('token')}`
            }
        })
        .then(res=>res.json())
        .then(data=>{
                setDetails(data);
            //   console.log(data.theme);
              setSelectedColor(data.theme);
              setDivStyle({
                  backgroundColor: data.theme,
              });
        })
    },[details])

    if (details.userImg === "")
    {details.userImg="https://cdn.dribbble.com/users/9685/screenshots/997495/avatarzzz.gif"}

    
    return (
      
    (details.id !== null)?
    <div id='profile' style={divStyle}>     
        <div id="profile-theme">
      
        </div>
        <Container fluid>
        <Row>
            <Col className='d-flex align-items-center blur-content-no-border py-3 my-5'>
                <Card.Img id="profile_img" className='d-flex align-content-center' src={details.userImg} />
            </Col>
            </Row>
            <Row>
            <Col xs={12} sm={12} md={{ span: 8, offset: 2 }} lg={{ span: 8, offset: 2 }} xl={{ span: 4, offset: 4 }}>
                <Card>
                    <CardHeader>
                        <h1 className='display-4 text-dark text-center pt-3'>{details.firstName} {details.lastName}</h1>
                    </CardHeader>
                    <Card.Body>
                        <ul className='list-unstyled'>
                            <li className='h5 text-secondary'>Contacts:</li>
                            <li>Mobile Number: {details.mobileNo}</li>
                            <li>Email: {details.email}</li>
                            <li className='h5 text-secondary'>Details:</li>
                            <li>Age: {details.age}</li>
                            <li>Address: {details.address}</li>
                        </ul>
                    </Card.Body>
                    <Card.Footer>
                        <div className='d-flex justify-content-end'>
                            <div className='me-2'><UpdateProfile /></div>
                            <div><ResetPassword /></div>
                            <div className='ms-2'><ProfileTheme
                                    selectedColor={selectedColor}
                                    handleColorChange={handleColorChange}
                                /></div>
                        </div>
                    </Card.Footer>
                </Card>
            </Col>
        </Row>
    </Container>
    </div>
		:
		<>
            <div className="text-center">
                <h1>Page is not accessible.</h1>
            </div>

        </>
    )
}