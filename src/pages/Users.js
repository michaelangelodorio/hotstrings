// src/components/UserList.js
import React, { useEffect, useState, useContext } from 'react';
import { Container, Row, Col, CardGroup } from 'react-bootstrap';
import UserContext from '../UserContext';
import AdminViewUsers from '../components/admin/AdminViewUsers';
import UserView from '../components/user/UserView';
import ProductView from './ProductView';

const Users = () => {
  const [users, setUsers] = useState([]);
  const { user } = useContext(UserContext);
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState(null);

  let token = localStorage.getItem('token');
  
  const fetchUserData = () => {
    
    fetch(`${process.env.REACT_APP_API_URL}/users/all`,{
      headers: {
        "Content-Type": "application/json",
        'Authorization': `Bearer ${token}`
      },
    })   
      .then((res) => res.json())
      .then((data) => {
        setUsers(data);
        // console.log("From Users's data: ", data);
      });
  }

  useEffect(() => {
    // Simulate AI processing
    setTimeout(() => {
      // Fetch data or perform AI-related tasks here
      fetchUserData();
      setIsLoading(false);
    }, 30); // Simulate 2 seconds of processing time
  }, []);

const renderView = () => {
    if (user.isAdmin) {
return (
    <Container fluid="true" >
    <Row>
      <Col>
        <AdminViewUsers users={users} fetchUserData={fetchUserData} />;
      </Col>
      </Row>
    </Container>
    )
    } else {
      return (
        <>
    <Container fluid="true" className='bg-custom'>
      <Row>
        <Col>
          <UserView users={users} user={user} fetchUserData={fetchUserData} />
        </Col>
      </Row>
    </Container>
    </>
)}}   
    
    return (
      <div>
      {isLoading ? (
        <div className="bg-dark centered-container">
        <img width={100}
          src="https://usagif.com/wp-content/uploads/loading-12.gif"
          alt="Centered Image"
          className="centered-image"
        />
      </div>
      ) : (
        <div>
        {renderView()}
        </div>
      )}
    </div>
    )
    };

export default Users;
