import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Row, Col, Form, Button, Stack, Modal, Container, InputGroup} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import { Navigate, useNavigate } from 'react-router-dom';

export default function Register() {

	const { user, setUser} = useContext(UserContext);
  const [openModal, setShowModal] = useState(false);
  const navigate = useNavigate();  
	const [ firstName, setFirstName ] = useState("");
	//add state hooks for lastName, email, mobileNo, password, confirmPassword, isActive(button)
	const [lastName,setLastName] = useState("");
  const [age, setAge] = useState(0);
  const [email,setEmail] = useState("");
  const [mobileNo,setMobileNo] = useState("");
  const [address,setAddress] = useState("");
  const [userImg,setUserImg] = useState("");
  const [password,setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");	
//State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);
  //fetch

    const showModal = () =>{
     setShowModal(true);
   }
  
    const closeModal = () =>{

      setShowModal(false);
    }


    function registerUser(e){
    	//prevent page redirection via form submission
    	e.preventDefault();

      if (userImg ===""){
        setUserImg("https://cdn.dribbble.com/users/9685/screenshots/997495/avatarzzz.gif")
      }

    	fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
    		method: 'POST',
    		headers:{
    			"Content-Type":"application/json"
    		},
    		body: JSON.stringify({

    			firstName: firstName,
    			lastName: lastName,
          age : age,
    			email: email,
    			mobileNo: mobileNo,
                address: address,
    			password: password,
                userImg: userImg
    		})
    	})
    	.then(res=>res.json())
    	.then(data=>{

    		if(data){
    			setFirstName('');
    			setLastName('');
          setAge('');
    			setEmail('');
    			setMobileNo('');
          setAddress('');
    			setPassword('');
          setUserImg('')
    			setConfirmPassword('')
    		
                Swal.fire({
                    title: "Registration successfull",
                    icon: "success",
                    text: "Welcome to HotStrings!"
                })
             
                navigate('/'); // Redirect to login page  
                closeModal();   
            } else {
        
                Swal.fire({
                    title: "Authentication Failed.",
                    icon: "error",
                    text: "Check your login details and try again."
                })
          
            }
    	}, [])
    }

    const resetForm=(e)=>{
        e.preventDefault();
        setFirstName('');
        setLastName('');
        setAge('');
        setEmail('');
        setMobileNo('');
        setAddress('');
        setPassword('');
        setUserImg('')
        setConfirmPassword('')
    }

    useEffect(()=>{


    if((firstName !== "" && lastName !== "" && email !=="" && mobileNo !== "" && password !=="" && confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)){

      setIsActive(true)

    }else{
      setIsActive(false)
    }

  },[firstName,lastName,age,email,mobileNo,password,confirmPassword])
  
return (
    <>
      {user.id !== null ? (<Navigate to="./" />) : (<>
        
        <Container fluid id="banner"
            className='d-flex min-vh-100 mt-5 justify-content-center align-items-center'
          >
            <Row className='text-white mt-2 mb-5'>
              <Col
                xs={{ span: 12, offset: 0 }}
                md={{ span: 12, offset: 0 }}
                lg={{ span: 12, offset: 0 }}
                xl={{ span: 12, offset: 0 }} className='p-5 blur-content-half-bg-dark'
              >
  
            <h6 className="text-center text-white mt-4">Register now to order!</h6>
            
              <Form onSubmit={(e) => registerUser(e)}>
              <Form.Label>Full Name: <span className='text-warning'>[Required]</span></Form.Label>
                    
                  <Form.Group className="mt-3">
                    <div className="d-flex">
                      <Form.Control
                        type="text"
                        placeholder="First Name"
                        required
                        value={firstName}
                        onChange={(e) => setFirstName(e.target.value)}
                        style={{ marginRight: '10px', flex: 1 }}
                      />
                      <Form.Control
                        type="text"
                        placeholder="Last Name"
                        required
                        value={lastName}
                        onChange={(e) => setLastName(e.target.value)}
                        style={{ flex: 1 }}
                      />
                  

                    </div>
                </Form.Group>
                <Form.Group className="mt-3">

                <Form.Label>Age: <span className='text-warning'>[Required]</span></Form.Label>
                       <Form.Control
                        type="number"
                        placeholder="Age"
                        required
                        value={age}
                        onChange={(e) => setAge(e.target.value)}
                        style={{ flex: 1 }}
                      />

                </Form.Group>

                <Form.Group className="mt-3">
                <Form.Label>Mobile No: <span className='text-warning'>[Required]</span></Form.Label>
                  <InputGroup className="mb-3">
                    <InputGroup.Text id="basic-addon1">#</InputGroup.Text>
                    <Form.Control
                        type="text"
                        placeholder="Enter 11-digit No."
                        required
                        value={mobileNo}
                        onChange={(e) => setMobileNo(e.target.value)}
                    />
                  </InputGroup>
                </Form.Group>
  
                <Form.Group className="mt-3">
                  <Form.Label>Home/Office Address:</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter Billing/Home Address"
                    required
                    value={address}
                    // value=" "
                    onChange={(e) => setAddress(e.target.value)}
                  />
                </Form.Group>
  
                <Form.Group className="mt-3">
                  <Form.Label>Picture: <span className='text-info'>[optional]</span></Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter Image URL [optional]"
                    value={userImg}
                    onChange={(e) => setUserImg(e.target.value)}
                  />
                </Form.Group>
           
                <Form.Group className="mt-3">
                <Form.Label>Email: <span className='text-warning'>[Required]</span></Form.Label>
                  <InputGroup className="mb-3">
                    <InputGroup.Text id="basic-addon1">@</InputGroup.Text>
                    <Form.Control
                    type="email"
                    placeholder="Enter Email"
                    required
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    />
                </InputGroup>
              </Form.Group>

                <Form.Group className="mt-3">
                  
                  <Form.Label>Password:  <span className='text-warning'>[Required]</span></Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Enter Password"
                    required
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </Form.Group>
  
                <Form.Group className="mt-3">
                  <Form.Label>Confirm Password:  <span className='text-warning'>[Required]</span></Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Confirm Password"
                    required
                    value={confirmPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                  />
                </Form.Group>
                <div className="d-flex justify-content-center mt-2">
                {
                    isActive ?(
                
                    <Stack className='d-grid my-4' direction="horizontal" gap={3}>
                      <Button style={{ width: 250 }} className='rounded-5' variant="primary" type="submit" id="submitBtn">Submit</Button>
                      <Button style={{ width: 250 }}className='rounded-5 my-1' variant="danger" type="reset" onClick={resetForm} id="resetBtn">Reset</Button>
                    </Stack>
                   )
                    :
                  (
                    <Stack className='d-grid mt-5' direction="horizontal" gap={3}>
                      <Button style={{ width: 250 }} className='rounded-5' variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
                      <Button style={{ width: 250 }} className='rounded-5 my-1' variant="danger" type="reset" onClick={resetForm} id="resetBtn">Reset</Button>

                      <h6 className="text-center text-white mt-4">Already have an account?</h6>
                      <Link style={{ width: 250 }} className="rounded-5 btn btn-warning d-block" to={'/users/login'}>Login</Link>  
                      
                    </Stack>
                  )
                }
                </div>
                <div className='py-5'>
                  By signing up, you agree to the <Link>Terms of Service</Link> and{' '}
                  <Link>Privacy Policy</Link>, <Link>including Cookie Use</Link>.
                </div>
                </Form>

                </Col>
                </Row>
              </Container>
       
        </>
      )}
    </>
  );
}