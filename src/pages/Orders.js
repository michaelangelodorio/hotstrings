import React, { useEffect, useState, useContext } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import UserOrders from '../components/user/UserOrders';
import AdminViewOrders from '../components/admin/AdminViewOrders';

export default function Orders() {
  const { user } = useContext(UserContext);
  const [order, setOrder] = useState([]);

  let token = localStorage.getItem('token');

  const fetchOrderData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/orders`, {
      headers: {
        "Content-Type": "application/json",
        'Authorization': `Bearer ${token}`
      },
    })
    .then((res) => {
      if (!res.ok) {
        throw new Error('Network response was not ok');
      }
      return res.json();
    })
    .then((data) => {
      setOrder(data);
   
    })
    .catch(error => {
      console.error('Error fetching orders:', error);
     
    });
  }

  useEffect(() => {
 
      fetchOrderData();
 
  }, [order]);

  return (
      <div>
       {user.isAdmin ? (
          <Container fluid="true" className='bg-custom mt-5'>
             <Row>
               <Col>
                 <AdminViewOrders order={order} fetchOrderData={fetchOrderData} />
               </Col>
             </Row>
           </Container>
         ) : (
        <Container fluid="true" className='bg-custom mt-5'>
          <Row>
            <Col>
              <UserOrders order={order} fetchOrderData={fetchOrderData}/>
            </Col>
          </Row>
        </Container>
         )
       }
      </div>
  )
}
