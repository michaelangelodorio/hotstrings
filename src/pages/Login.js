import React, { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Stack, Button, Modal, Form, FormCheck } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Navigate, useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';

export default function Login() {

    // Allow us to continue the User context object and it's properties to
    const {user, setUser} = useContext(UserContext);
    // const [showEdit, setShowEdit] = useState(false);
    const navigate = useNavigate();
    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);
    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState(null);

    const [formData, setFormData] = useState({
      email: '',
      password: '',
      rememberMe: false
    });
    
  const authenticate = (e) => {

   
    
    // Prevents page redirection via form submission
    e.preventDefault();
    
    const authData = {
        email: email,
        password: password
    };


    const { name, value, type, checked } = e.target;
    setFormData(prevState => ({
      ...prevState,
      [name]: type === 'checkbox' ? checked : value
    }));


    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(authData)
    })

    .then(res => res.json())
    .then(data => {

    if(typeof data.access !== "undefined"){
        localStorage.setItem('token', data.access);
        // localStorage.setItem('email',email);
        retrieveUserDetails(data.access); //

        setUser({
            access : localStorage.getItem('token'),
            // email : localStorage.getItem('email')
    });  

        Swal.fire({
            title: "Login successful",
            icon: "success",
            text: "Welcome to HotStrings!"
        })
            navigate('/users/login'); // Redirect to login page  
        // alert(`You are now logged in`);


    } else {

        Swal.fire({
            title: "Authentication Failed.",
            icon: "error",
            text: "Check your login details and try again."
        })
        // alert(`${email} does not exist`)
  
    }
})
    // Clear input fields after submission
    setEmail('');
    setPassword('');
    //SetConfirmPassword('');
}

    const retrieveUserDetails = (token) => {
      
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            method: 'GET',
            headers: {
                Authorization : `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(data => {
                  
            setUser ({
                id: data._id,
                isAdmin: data.isAdmin,
                userImg: data.userImg
            })
        })
    }

  useEffect(() => {
    // Simulate AI processing
    setTimeout(() => {
      // Fetch data or perform AI-related tasks here
      if(email !== '' && password !== ''){
        setIsActive(true);
    }else{
        setIsActive(false);
    }

      setIsLoading(false);
    }, 30); // Simulate 2 seconds of processing time
  }, [email, password]);

  return (
    <div>
    {isLoading ? (
      <div className="bg-dark centered-container">
      <img width={100}
        src="https://usagif.com/wp-content/uploads/loading-12.gif"
        alt="Centered Image"
        className="centered-image"
      />
    </div>
    ) : (
      <div>
       {user.id !== null ? (
          <Navigate to="/products" />
      ) : (
        <>
          <Container fluid id="banner"
            className='d-flex min-vh-100 pt-5 justify-content-center align-items-center'
          >
            <Row className='text-white mt-2 mb-5'>
              <Col
                xs={{ span: 12, offset: 0 }}
                md={{ span: 12, offset: 0 }}
                lg={{ span: 12, offset: 0 }}
                xl={{ span: 12, offset: 0 }} className='p-5 blur-content-half-bg-dark'
              >
                <h1 className='text-center text-white'> <span className="textfx"> Login</span></h1>
                
                <Form onSubmit={(e) => authenticate(e)}>
                  <Form.Group controlId='userEmail'>
                    <Form.Label>
                      <span className="textfx">E</span>
                      <span className="logo">/MAIL</span>
                    </Form.Label>
                    <Form.Control
                      type='email'
                      placeholder='Enter email'
                      value={email} // Bind to email state
                      onChange={(e) => setEmail(e.target.value)}
                      required
                    />
                  </Form.Group>
  
                  <Form.Group className='mt-3' controlId='password'>
                    <Form.Label>
                      <span className="textfx">PASS</span>
                      <span className="logo">/WORD</span>
                      </Form.Label>
                    <Form.Control
                      type='password'
                      placeholder='Password'
                      value={password} // Bind to password state
                      onChange={(e) => setPassword(e.target.value)}
                      required
                    />
                  </Form.Group>
  
                  {isActive ? (
                    <Button className='my-3' variant='primary' type='submit' id='submitBtn'>
                      Submit
                    </Button>
                  ) : (
                    <Button className='my-3' variant='danger' type='submit' id='submitBtn' disabled>
                      Submit
                    </Button>
                  )}
 
                <Row className="mb-4">
                  <Col className="d-flex justify-content-center">
                    <Form.Check
                      type="checkbox"
                      id="rememberMe"
                      name="rememberMe"
                      checked={formData.rememberMe}
                      label="Remember me"
                      onChange={(e) => setFormData(e.target.value)}
                    />
                  </Col>
                  <Col>
                    <a href="#">Forgot password?</a>
                  </Col>
                </Row>

                <div className="text-center">
                  <p>Not a member? <a href="/users/register">Register</a></p>
                  <p>or sign up with:</p>
                  <Button variant="secondary" className="btn-floating mx-1">
                    <i className="fab fa-facebook-f"></i>
                  </Button>
                  <Button variant="secondary" className="btn-floating mx-1">
                    <i className="fab fa-google"></i>
                  </Button>
                  <Button variant="secondary" className="btn-floating mx-1">
                    <i className="fab fa-twitter"></i>
                  </Button>
                  <Button variant="secondary" className="btn-floating mx-1">
                    <i className="fab fa-github"></i>
                  </Button>
                </div>
                </Form>

              <Col className="d-flex justify-content-center">
                <div className='py-5'>
                By signing up, you agree to the <Link>Terms of Service</Link> and{' '}
                <Link>Privacy Policy</Link>, <Link>including Cookie Use</Link>.
                </div>
              </Col>

              </Col>
            </Row>
          </Container>
        </>
      )}
      </div>
    )}
  </div>
  );     
}
