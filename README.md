HotStrings - Capstone 3

# E-COMMERCE APPLICATION
Deployed at Vercel.com
- https://mdorio-capstone3.vercel.app/

## Features:
- Register Page
- Login Page
- Admin Dashboard
    - Create product
    - Retrieve all products
    - Update Product Information
    - Deactivate/Reactivate product

- User Catalog
    - Search for products
        - Retrieve all products
        - Search by Price Range
        - Search by Name
     - Checkout Order
        - Non-admin User checkout (Create Order)

- Stretch Goal
    - Order History Page
        - Retrieve all auth user's orders
        - Retrieve all orders (Admin)
        - Set role from user to Admin (Admin)
        - Others, See changelog for more information.

<details>
<summary>
TEST ACCOUNTS
</summary>

- Test User 01:
    - email: ada@mail.com
    - pwd: 1234

- Test User 02
    - email: l@mail.com
    - pwd: 1234

- Admin User:
    - email: sudo@mail.com
    - pwd: sudo1234
</details>

<details>
<summary>
UPDATES
</summary>

- Change Log:
    - Admin to view all orders (history) from all users
    - Admin can set activate user accounts (Admin)
    - User is able to view their order history
    - Users can search by name and price ranges
    - A detailed view page for each product
    - Added "Featured products" and "Must try" products
    - Featured products were randomly shown by batch
    - View and manage all users from the admin view
    - View and manage all orders from the admin view
    - View and manage all products from the admin view
    - User can change their order status to complete and cancel
    - Users can update the profile and Change the password on the profile page
    - A default GIF will be set if the image profile picture is blank
    - Admin change customer/user's order status, from order . . . delivery
    - User and Admin can change order status
    
    - Products can be moved to "Like"s.
    - Only Items/Products from the "Cart" are being pushed for checkout.
    - Products in the "Likes" are not included when the user does a checkout
    - Added Icons, logout, profile, and device icon for user resolution.
    - Guests can only view comments, users from the comment section will be masked as "Anon User", view product details, and browse.
    - Guests are required to log in for any other action than browsing.
    - Added Contact Page
    - Added remove button for "Cart" items
    - Added Order Pending Status text
    - Enhanced Product Preview page
    - Change rate into stars (★★★★★), comment and rate is now displaying instantly.
    - Fixed the default image upon registration. See profile Icon at the bottom of the page.
    - Fixed and enable the address field from the profile page.
    
    - Updated Profile, User can change their profile picture, background color (optional)
    - Changes in Admin Dashboad for users, naming and caption to avoid confusion.
    - Once Review/Rate added, a floating notification on the top-left of the screen will be displayed.
    - Button added in Product view, requires Login to rate.
    - Update visuals for Orders, added static QR, business logics for Payment, Complete and Cancel Order.
    - Added a real-time user interaction, by cancelling and completing the order.
    - Admin will see the updates if user cancel/complete the order.
    - Added link for each item from "Cart" and "Likes", linked to its product description.
    - Update clear cart, which only those items in the "Cart" will be cleared. Liked items will be excluded from clearing.
    - Once an item added to the cart, available products/stocks are now deducted. Only Admin can update the items count.
    - Add Delivery Address at the Admin's Manage Orders page
    - Added profile image to comment section, it will also update if the user change the image.

- Future Plans:
    - To display all contents in the Mobile version (Work in progress)
    - Clear "Cart" feature added, including "Likes" (Work in progress)
    - Product Rating and Comment Added (Work in progress for dynamic updates)
    - Badges for "Cart" and Orders (Work in progress for dynamic updates)
    - Add buttons to decide if cust want their order(s) to be canceled or complete from their end.
    - To Add a Search feature for Orders and Products
    - Study more related to FetchData, and practice writing clean code. Eat. Sleep.
</details>
